FOLIO 15r
ET MIHI CONSPICUUM MERITIS BELLOQUE tremendum
Musa uirum referes italis cui fracta sub armis
Nobilis eternum prius attulit affrica nomen
Hunc precor exhausto liceat mihi suggere fontem
Ex elicone sacrum dulcis mea cura sorores
Si uobis miranda cano iam ruris amici
Prata quidem et fontes uacuisque silentia campis
Fluminaque et colles et apricis ocia siluis
Restituit fortuna mihi uos carmina uati
Reddite uos animos tuque / o / certissima mundi
Spes superumque decus quem secula nostra deorum
Victorem atque herabi memorant quem quina uidemus
Larga per innocuum retegentem uulnera corpus
Auxilium fer summe parens tibi multa reuertens
Vertice parnasi referam pia carmina si te
Carmina delectant uel si minus ista placebunt
Forte etiam lacrimas quas mens sic fallitur olim
Fundendis longo demens tibi tempore seruo
Te quoque trinacrij moderator maxime regni
Hesperieque decus atque eui gloria nostri
Iudice quo merui uatumque in sede sedere
Optatasque diu Lauros titulumque poete
Te precor oblatum tranquillo pectore munus
Hospitio dignare tuo nam cuncta legenti
Forsitan occurret uacuas quod mulceat aures
Peniteatque minus suscepti in fine laboris
Preterea in cunctos pronos sic feceras annos
Posteritatis iter Quis enim dampnare sit ausus
FOLIO 15v
Quod uideat placuisse tibi fidentius ista
Arguit expertus nutu quem simplice dignum
Effecisse potes quod non erat aspice templis
Dona sacris affixa pauens ut uulgus adoret
Exime despiciet quantum tua clara fauori
Nominis ista tui durum spretura uenenum
Inuidie latuisse uelis ubi nulla uetustas
Interea et nulli rodent mea nomina uermes
Suscipe iamque precor regum inclite suscipe tandem
Atque pias extende manus et lumina flecte
Ipse tuos actus meritis ad sidera tollam
Laudibus atque alio fortassis carmine condam
Mors modo me paulum expectet non longa petuntur
Nomen et alta canam siculi miracula regis
Non audita procul sed que modo uidimus omnes
Omnia nanque solent similis quos cura fatigat
Longius isse retro tenet hos millesimus annus
Sollicitos pudet hac alios consistere metam
Nullus ad etatem propriam respexit ut erret
Musa parum notos nullo prohibente per annos
Liberior troiamque ideo canit ille ruentem
Ille refert thebas iuuenemque occultat acchillem
Ille autem emathiam romanis ossibus implet
Ipse ego non nostri referam modo temporis acta
Marte sed ausonio sceleratos funditus afros
Eruere est animus nimiasque retundere uires
At semper te corde gerens properansque reuerti
Rex iter hoc ingressus agam tua maxima facta
Non ausus tetigisse prius magis illa trahebant
Sed tremui meque te uidens atque omnia librans
Ingnenium temptare libet si forte secundis
FOLIO 16r
Cesserit auspiciis solidis tunc uiribus alta	
Aggrediar nanque ipse aderis meque ampla uidebunt
Inclita parthenope redeuntem ad menia rursum
Et Romana iterum referentem serta poetam
Nunc teneras frondes humili de cespite uulsi
Scipiade egregio primos committante paratus
Tunc ualidos carpam ramos tu nempe iuuabis
Firmabis meritumque decus continget amanti
Altera temporibus pulcerima laurea nostris		
Que tantis sit causa malis que cladis origo
Queritur unde animi quis tot tollerare coegit
Dura pererrato ualidas furor equore gentes
Europamque dedit libie libiamque rebellem
Europe alterno uastandas turbine terras.
Ac mihi causa quidem studij non indiga longi
Occurrit radix cunctorum infecta malorum
Inuidia unde oriens extrema ab origine mors est
Atque aliena uidens tristi dolor omnia uultu
Prospera non patuit florentem cernere romam
Emula cartago surgenti inuiderat urbi
Sed grauius tulit inde parem mox uirbius auctam
Vidit et imperio domine parere potentis
Ac leges audire nouas et ferre tributum
Edidicit tacitis intus sed plena querelis
Plena nimis frenum funesta superbia tandem
Compulit excutere et clades geminare receptas
Angebant dolor atque pudor seruilia possos
Multa uiros animisque incesserat addita duris
FOLIO 16v
Tristis auaritia et nunquam satiabile uotum	
Permixte spes amborum optatumque duobus
Imperium populis dignus sibi quisque uideri
Omnia cui subsint totus cui pareat orbis
Preterea dampnumque recens iniuriaque atrox
Insula sardinie amissa et trinacria rapta
Atque hispana nimis populo confinis utrique
Omnibus exposita insidiis aptissima prede
Terra tot infandos longum passura labores
Haud aliter quam cum media deprehensa luporum
Pinguis ouis nunc huc rapidis nunc dentibus illuc
Voluerit inde tremens partes discerpitur omnes
Bellantum proprioque madens resupina cruore
Accessit situs ipse loci natura locauit
Se procul aduero spectantes lictere gentes
Aduersosque animos aduersas moribus urbes
Aduersosque deos odiosaque numina uterque/utrique
Pacatique nihil uentos elementaque prorsus
Obiuia et infestos luctantes equore fluctus
Ter grauibus certatum odijs et sanguine multo
At ceptum primo profligatumque secundo
Est bellum si uera notes nam tertia nundis
Prelia finis habet modico confecta labore
Maxima nos rerum hic sequimur mediosque tumultus
Eximiosque duces et inenarabile bellum
Vltima sidereum juuenem laxata procellis
Hesperia excussamque graui ceruice cathenam
Ausoniumque iugum romanaque senserat arma
Iam fuga precipites longe trans equora penos
Egerat horruerant animos dextrumque tonantis
Fulmineam moresque ducis famamque genusque
FOLIO 17r
Armorumque nouas artes atque orsa cruentis
Nobilitata malis uix tandem littere mauro
Perfidus urgentem respectans hasdrubal hostem
Tutus erat sic uenantum perterritus acrem
Respicit atque canum ceruue post terga tumultum
Montis hanella procul de uertice colla reflectens
Constitit occeano domitor telluris hibere
Qua labor ambiguus uatum pellagique columnas
Verberat herculeas ubi fessus mergitur alto
Phebus et estiuo detergit puluere currum
Hic ubi uis ulla manu mortalis at ipsa
Omnipotens aduersa aditum natura negabat
Constitit atque auidis preceptum faucibus hostem
Indoluit uicisse parum iam blandior egrum
Non mulcet fortuna animum cartagine recta
Gloria gestarum sordebat fulgida rerum
Nempe uidebat adhuc profugum longinqua tuentem
Lentaque seminaci uibrantem spicula dextra
Turbida cum etiam rumoribus omnia miscens
Fama procul nostro ueniens crescebat ab orbe
Arcibus instantem auxoniis uolitare sub armis
Hanibalem patrieque faces sub menia ferri
Illustres cecidisse duces ardere nefandis
Ignibus hesperiam atque undantia cedibus arua
Vrgebat uindicta patris pietasque monebat
Vt ceptum sequeretur opus nam sanguine seuo
Cesarum cineresque sacros unbrasque parentum
Placari atque itala detergi fronte pudorem
Hic amor assiduum pulsabat pectora clari
Scipiade infantem eliciens oculosque juuenta
Fulgentes calido generosos corde fauillas
FOLIO 17v
Anxia nox operosa dies uix ulla quietis
Hora duci tanta indomito sub pectore uirtus
Has inter curas ubi sensim amplexibus atris
Nox unda laxabat humum titonia quamuis
Vxor adhuc gelidumque senem complexa foueret
Nec dum purpureo nitidas a cardine ualuas
Vellere seu roseas ause reserare fenestras
Excierent dominam famule que secula uoluunt
Fessus et ipse caput posuit tunc lumina dulcis
Victa sopor clausit celoque emissa silenti
Vmbra ingnens faciesque patris per nubila raptim
Astitit ostendens cara precordia nato
Et latus et multa transfixum cuspide pectus
Diriguit totos iuuenis fortissimus artus
Arrecteque orrore come tunc ille pauentem
Corripit et nato permulcens incipit ore
O decus eternum generisque certissima nostri
Gloria et o patrie tandem spes una labantis
Siste metum memorique animo mea dicta reconde
Optimus ecce breuem sed que nisi despicis horam
Multa ferat placitura dedit moderator olimpi
Ille meis uictus precibus stellantia celi
Lumina perrarum munus patefecit et ambos
Viuentem penetraret polos permixit ut astra	
Me duce et obliquos calles patrieque labores
Atque tuos et adhuc terris ingnota sororum
Stamina tum rigido contortum pollice fatum
Aspicias huc flecte animum uiden illa sub austro
Menia et infamia periura palatia montem
Femineis fundata dolis uiden ampla furentum
Concilia et tepido stillantem sanguine turbam
FOLIO 18r
Heu nimium nostris urbs insignita ruinis
Heu nuribus trux terra italis iterum arma retentas
Fracta semel uacuisque iterum arma retentas
Sic tibrim indomitum segnissime barhadra temnis
Sic modo birsa ferox capitolia despicis alta
Experire iterum et dominam per uerbera nosces
Is tibi nate labor superem ea gloria iusto
Marte parem factura deis heu uulnera uiro
Sacra mihi merito patrie quibus omne refudi
Quod dederat quibus ad superos mauortia uirtus
Hostibus atque abeunte anima mihi multa dolenti
Occurrisse prius tanti solamina casus
Quam quam magnanimum post funera nostra uidebam
Vltorem superesse domi spes ista leuabat
Inde metus alios hinc sensum mortis amare
Talia narrantem percurrit et impia mestis
Vulnera luminibus totumque auertice corpus
Lustrat adusque pedes at mens prominet extra
Vbertimque fluunt lacrime nec plura parentem
Sustinuit mediisque irrumpens uocibus orsus
Heu heu quid uideo quisnam mihi pectora
Confixit mucrone parens que dextra uerentem
Gentibus immerito uiolauit sanguine frontem
Dic genitor nil ante queas committere nostris
Auribus hec dicens alto radiantia fletu
Sidera uisus erat sedesque implesse quietas
Infima si liceat summis equare marina
Piscis aqua profugus fluuioque repostus ameno
Non aliter stupeat si iam dulcedine captum
Vis salis insoliti et subitus circumstet amores
Quam sacer ille corus stupuit nanque hactenus ire     	
FOLIO 18v
Et dolor et gemitus et mens incerta futuri
Atque metus mortis mundique miserrima nostri
Milia curarum rapide quibus optima uite
Tempora et intenebris meliores ducimus annos
Illic pura dies quam lux eterna serenat
Quam nec luctus edax nec tristia munera turbat
Non odia intendunt noua res auremque deorum
Insuetus pulsare fragor pietate recessus
Lucis in accesse tacitumque inpleuerat axem
At pater amplexu cupido precibusque modestis
Occupat et grauibus cohibet suspiria dictis
Parce precor gemitu non hunc tempusque locusque
Exposcunt sed uisa animum si uulnera tangunt
Vsque adeo iuuat et patrios agnoscere casus
Accipe nam paucis perstringam plurima uerbis.
Sexta per experios penitus uictricia campos
Nostraque signa simul romanoque uidera estas
Cum mihi iam bellique moras curasque peroso
Consilium teste euentu fortuna dedisti
Magnificum infelix fido uicum fratre uiritim
Solicitum partire honus geminumque moranti
Incuterem bello calcar sic alite laua
Distrahimur tandem et scissis legionibus ambo
Insequimur late sparsis legionibus hostes
Nondum plena colis iam stamina nostra sorores
Destituunt fosse iam mors sua signa relinquit
Illicet imparibus ueritis concurrere fatis
Fraudis opem dubio poscunt in tempore peni
Ars ea certa uiris et nostro cognita damno
Celtarumque animos quibus auxiliaribus arma
Fratris ad id steterant precio corrumpere adorti
FOLIO 19r
Persuasere fugam nostrorum exempla pereunt
Ante oculos gestanda ducum ne robore freti
Eterno proprio non plus inmilite fidat
Obicit ille deos uis fas et inania uerba
Raptim abeunt tacitoque uale uis quanta metallo est
Dij pudor alma fides uni succumbitis auro
Presidio nudata acies fraterna retrorsum
Auia constituit notosque recurrere montes
Hec uisa est spes una duci premit hostis acerbus
Doctus ad extremum cedenti insistere tergo
Me quoque iam magno distantem punica tractu
Agmina cignebant que clam nouus auxerat hostis
Improbus insultans uisum et mihi cedere fato
Nequicquam uetitum caro me iungere fratri
Inferior numero multum tribus undique castris
Vallabar multumque locis urgebar iniquis
Ferrum aderat spes nulla fuge quod fata sinebant
Tempore in angusto durissima pectora ferro
Pandimus et uafras herebo detrudimus umbras
Ira dolorque dabant animos ars bellica nusquam
Consilijque nihil ceu dum uelamina pastor
Fida gerens apibus bellum mouet inprobus almis
Nocte sub oscura trapidant mox dulcia meste
Excedunt inopi substrata cubilia cere
Inde ruunt ceceque fremunt sparsoque uolatu
Importuno instant capiti stat calidus hostis
Inceptique tenax post irrita uulnera uictor
Eruit extirpatque pie cunabula gentis
Sic que sola salus miseris etiam summa uoluntas
Inuisam iaculis gladioque ultore cohortem
Tundimus et rapidas in uulnere linquimus iras
FOLIO 19v
Illi ex conposito stabant ceu flantibus austris
Aerius consistit erix atque astriger athlas
Quid moror incauti armorum sub nube uirumque
Obruuntur fortuna suum tenet inuida morem
Adduersata pios gelidus mihi pectore sanguis
Heserat agnosco insidias mortemque propinquam
Nec mihi sed patrie metuens pro tempore raptim
Ingredior dictis cuneas firmare labantes
Hac uia preclari miles patet ardua leti
Iudice me quem sepe alias maiore profecto es
Fortuna nunquam fama meliore secutus
Non acies ferri facie non obuia mortis
Terreat exiguo decus ingnens sanguine mauors
Obicit et caros illustrat cede nepotes
Nosce genus patriamque libens amplectere fortem
Ingnauum fortemque mori non tangere dampno
Nature lex una iubet breue tempus utrinque
Iamque licet et terre pellegique pericula cessent
Vltro aderit suspecta dies hoc fortibus unum
Contingnit ut leti morientur cetera flendo
Turba perit lacrimasque metu diffundit inertes
Hora breuis longe testis uenit ultima uite
Ergo age si latio quicquam de sanguine restat
Morte palam facito nam dum fortuna sinebat
Vicimus et nostris exibant funera dextris
At modo corporibus cedunt quando omnia retro
Sit satis obstruxisse uiam per pectora nostra
Perque truces oculos uultusque inmorte tremendos
Transcendant talem libet his apponere montem
His claustris uallare aditus sciat orida ueros
Barbarios cecidisse uiros et pallida quamquam
FOLIO 20r
Haud spernanda tamen Romana cadauera calet
Accelera bene nata cohors in limine mors est
Inuidiosa bonis romanas semper ad auras
Cum lacrimis recolenda piis et thure perenni
Talibus accensi coetunt et grandinis instari
Scissa nube ruunt in tela micantia primus
Et circumfusos feror irrediturus in hostes
Consequitur deuota neci fortissima pubes
Sternimus et morimur paucis tot milia contra
Quid reliquum sed fata pij nunc ultima fratris
Expectas neque enim hesperia felicior ora
Ille quidem extremo fati de turbine frustra
Surgere conatus magne sub molle ruine
Oppressusque itidem nec mors magis ulla decebat
Altera quam fratris fuerat concordia uite
Mira uel exiguis nunquam interrupta querelis
Vna domus uictusque idem mens una duobus
Et mors una fuit locus idem corpora seruat
Amborum et cineres huc tempus forte sub unum
Venimus hic nobis nulla est iam cura venisti
Carceris ex alto sparsos contenimus artus
Odimus et laqueos et uincula nota timemus
Libertatis onus quod nunc sumus illud amamus
Ille ait lacrimans tua me tua care profundo
Corde premit pietas genitor sed mollis inersque
Vltio uerborum semper fuit ultima rerum
Dic tamen hoc /o/ sancte parens an uincere fratrem
Teque putem atque alios quos prides roma sepultos
Defunctosque uocat ente pater ipse loquentem
Risit et o quanta miseri sub nube latetis
Humanumque genus quanta caligine ueri
FOLIO 20v
Voluitur hec inquit sola est certamina uita.
Vestra autem mors est quam uitam dicitis at tu
Aspice germanum uiden ut contemptor acerbe
Mortis eat uiden indomitum sub pectore robur
Et uiuum decus et flamantia lumina fronti
Quin etiam atergo generosum respicis agmen
Hos mihi defunctos audebit dicere quisquam
Et tamen egregios humani sorte tributi
Efflauere animos et debita corpora terre
Liquerunt cernis nitido uenientia contra
Purpureum radiare diem leta agmina uultu
Iamque ait eximie nec quicquam dulcius unquam
Hos uidisse oculos memini sed nomina nosse
Est auis tibi ne genitor contraria mens sit
Per superos ipsumque jouem solemque uidentem
Omnia per frigios si qua est ea cura penates
Per siquid patrie uenit huc dulcissimus oro
Aaut ego fallor enim aut quosdam hoc ex sanguine noui
Est mores habitusque uirum faciesque gradusque
Insolitum licet ora micent tamen ora recordor
Vidi et enim et patria nuper conuiximus urbe
Vera quidem memoras ubi furta latebant
Punica terenis perijs congressus iniquo
Credulus etate heu nimium marcellus in illa
Iste memor finis lateris latus admouet ultro
Nobiscum ue libens celo spatiatur in amplo
Crispinus longe sequitur quem perfidus uno
Adsumpsisse die temptauerat hostis at illum
Languida dilate tribuerunt uulnera morti
Alter ibi cecidit moriens ubi furta latebant
Inde leuis recte pentrans huc spiritus illuc
FOLIO 21r
Frigida carnifici dimisit membra cruento
En fabium celo maiestas maxima tanti
Nominis ac rerum iubent hinc habitare sereno
Cerne ducem quantum licet hic cunctator ab omni
Dictus erat populo tamen ignens gloria tanti
Debita consiliis uiget hunc non flamma non ignis
Eripuit latio sed dum magna umbra premebant
Punica tranquillum tulit hunc animosa senectus
Sed magis ardentemque animis pugnamque trementem
Cerne per insidias indigno funere graccum
Corpore seculum ualido et pollentibus armis
Preterea Emilio nimium fors inuida paulo
Aspice magnanimum terebrant quod uulnera pectus
Canensi romana die defleta supremum
Fata putans renuit cladi superesse sed ultro
Oblatum contensit equum multumque rogantem
Repulit et nimium respondit uiximus at tu
Macte animi uirtute puer discede tuumque
Virtutum abde caput teque ad meliora reserua
Dic patribus muniant urbem dic menia firmet
Candiscant extrema pati namque inproba serues
Ingeminat fortuna minas hostisque cruentus
Victor adest fabio mea uerba nouissima profer
Dic me iussurum memorem uixisse suorum
Dic memorem te teste mori sed fata feroxque
Collega ingnenti turbarunt cuncta tumultu
Nuda loco caruit uirtus tulit impetus illam
Effuge dum morior ne forsam plura loquendo
Sim tibi causa necis dicentem talia ferro
Circunstant uolat ille leuis timor aleuat artus
FOLIO 21v
Et plumas adiungit equo et calcaria plantis
Anxia sic genitrix ubi nidum callidus anguis
Obsident hinc uise sese subducere morti
Optat et hinc dubitat sua dulcia uiscera linquens
Infelix pietas tandem formidine uicta
Cedit et intussis serum sibi consulit alis
Vicinaque timens respectat ab arbore fatum
Natorum rabiemque fere et plangoribus omne
Implet anella anella domus strepituque amico.
Sic ibat iuuenis memorandus sepe retrorsum
Lumina mesta ferens uidet ingnens surgere campis
Naufragium uidet imtem post publica penum
Funera sacra ducis fodientem pectora duris
Letibus et celum gemitu pulsabat inani
Talia dum genitor memorat suspiria natus
Alta trahens licuit fateor cognoscere quidquid
Optabam magis et uultus spectare meorum
Cetera ni prohibet nihil est sermone secundi
Patris amabilius quin tu modo quominus inquit
Aloquere atque aures quamprimum inuade paratas
His dictis tulit ante gradum frontemque modestam
Demisit patruumque tenens sic incipit ore
O uenerande mihi uero nunquamque parentem
Care minus si uera deus dedit ora uidere
Mortales oculos alti si lumina mundi
Indignoque mihi clarum reserauit olimpum
Da precor exiguam nostris affatibus horam
Nam breue tempus adest moneorque in castra reuerti
Occeani subnixa uadis ubi maxima calpe
Impendet pellago celumque cacumine pulsat
Illic me romana manent modo signa duceque
FOLIO 22r
Expectant rapidum hoc tandem stant limite bellum
Suscipit amplexu iuuenum placidissimus heros
Atque ita si iussu superum mortalia celo
Membra uehis nec enim tam magni muneris auctor
Alter erit summum hoc equidem tibi contigit uni
Eximiumque decus quam de te concipiam spem
Dictu dificile est cui tanti numina uiuo
Concesse uiam nam ni diuinus inesset
Spiritus haudquaquam hoc homini fortuna dedisset
Que faciles dispensat opes Arcana uidere
Celica uenturos longe prenoscere casus
Et fatum prescire suum spectare beatas
Has animas subterque pedes radiantia solis
Lumina et aduersos tam uastis tractibus axes
Hec numquam fortuna dabit quia cuncta potenti
Sunt seruata deo qui si te lumine tanto
Illustrat quonam te alii dignent honore
Non ergo inmerito fractos passimque iacentes
Hesperie campis totiens despeximus hostes
Vidimus et nostre uindictam mortis ab illa
Quid libet hic aude mecum nam protinus aurem
Iuuenis atque animum uacuum quin ocius ergo
Ingredimur fandoque breuem consumimus horam
Dic ait is si uita manet post busta quod almus
Testatur genitor sique hec est uera perennis
Nostro autem morti similis quod demoror ultra
In terris quin huc potius quacumque licebit
Euolat assurgnens animus tellure relicta
Non bene sentis ait deus hoc deus ordine sanxit
Legibus eternis hominem statione manere
Corporis edicto denec reuocetur aperto
FOLIO 22v
Non igitur properare decet sed ferre modeste
Quantulacumque breues supperant inconmoda uite
Nec uisum spreuisse dei uideare quod ista
Sunt geniti sub lege homines ut regnant tenerent
Infima namque illis custodia credita terre
Et rerum quas terra uehit pelagusque profundum
Ergo tibi cunctisque bonis seruandus in ista est
Carnem animus propriamque uetandus linquere sedem
Nobilibus curis studioque amore uidendi
Promineat ni forte foras corpusque relinquat
At longe fugiat sensus seque ingnerat astris
Hic decet egregios animos hic exitus est quem
Diuini fecere uiri meliora sequentes
Sed dum menbra uigent breuis est mora suscipe nostri
Consilij quod summa uelit tu sacra fidemque
Iustitiamque cole pietas sit pectoris hostes
Sancta tui morumque comes que debita uirtus
Magna patri patrie maior sed maxima summo
Ac perfecta deo quibus exornato profecto
Vita uia in celum est que nos huc tramitte recto
Tunc reuehat cum summa dies exemerit illud
Carnis onus pureque animam trasmiserit aure
Hoc etiam monuisse uelim nil gratius illi
Qui celum terrasque regit dominoque patrique
Actibus ex nostris quam iustis legibus urbes
Conciliumque hominum sociatum nexibus equis
Quis enim ingnenio patriam seu uiribus alte
Substulerit sumptisque oppressam iuuerit armis
Hic certum sine fine locum in regione serena
Expectet uereque petat sibi premia uite
Justicia statuente dei que nec ad inultum
FOLIO 23r
Nec premio caruisse sinit sic fatus amoris
Admouitque faces
habitus tamen omnibus unus
Sidereoque leuis fulgebat lumina amictus
Augusta pauci procul omne fronte preibat
Iam seruoque graues et magestate uerendi
Hec acies regum est quos tempora prima tulerunt
Vrbis autem nostre frons arguit inclita reges
Romulus ecce prior famosi nominis auctor
Publicus ille parens demis dulcissime quantus
Ardor inest animo talem uentura petebat
Regna uirum uenit incessu moderatior alter
Religione noua populum qui temperet arcem
Hic uirtute prius patrie curibusque sabinis
Insignis nostramque ideo transuersus in arcem est
Aspice solicitum monitu ceu coniugis almos
Instruat leges et euntem diuidat annum
Extulit hunc natura senem primoque sub euo
Hanc habuit frontem sic tempora cana genalque
Tertius ille sequens qua tu nunc uteris omnem
Militie expressit Regum fortissimus artem
Fulmineis uisu uictus quoque fulmine solo
Quartus arat muros et tibridis hostia fundat
Presagus quas totus opes huc conuehat orbis
Biffida primeuo connectens menia ponto
Frons quinti mihi nota parum sed suspicor illum
Quem nobis regem longe dedit alta corinthus
Ille est haud dubie uideo tunicasque togatas
Et fasces trabeasque graues sellasque corillos
Atque leues phaleras et cunta insignia nostri
Imperij cursusque et equos pompasque triumphi
Illum autem numero quem cernis in ordine sextum
FOLIO 23v
Seruilis solio regem trasmisit origo
Et nomen seruile manet sed regia mens est
Dedecus hic generis uirtute priauit et actis
Condidit hic cesum prior ut se nouere posset
Roma potens altumque nihil sibi nota timeret
Finierat tunc ille iterat si lecta recordor
Romuleo cinxisse comas diademate septem
Audieram totidem cognomina certa tenebam
Alter ubi est igitur filij carissime dixit
Huc et luxus iners et dura superbia nunquam
Ascendunt illum sua pexima crimina auerno
Merserunt atroxque animus nomenque superbum
Hunc exposcis enim qui scetra nouissima nouit
Rex ferus et feritate bonus nam tristia passe
Hic libertatis primum urbi ingressit amorem
Quin tu animas letas melioraque regna tenentes
Cerne cateruatim uere uirtutis amicos
Tres sumus ante alios Alternaque brachia nexi
Ibant hos leto celebrabant agmina pulsu
Vmbrarum atque omni deuotum ex ordine uulgus
Sustitit ad muros que tanta est gratia dixit
Ista trium quis tantus amor connectit euntes
Hos idemque parens eademque ait extulit aluus
Huic amor hisque ipsis libertas credita quondam
Hinc fauor heu quam uigulos in uulnera cruda duorum
Aspice uterque recens nitor ut generosa cicadrix
Pectore in aduerso populorum pugna potentum
Tergeminis mandata uiris ut sanguine pauco
Scilicet in numero cessarent funera gentis
Diuis exercitibus conspecta suorum
Aduersusque oculis sex ultima bella gerebant
FOLIO 24r
Libertas tunc nostra tremens similisque cadenti
Vnius ad fatum dubio sub marte pependit
Occiderat populo nimis fortuna fauere
Ceperat albano nisi tertius ille superstes
Integer et fratrum mortes et publica fata
Restituisset agens uictricia corpora campo
Donec se uinctos spacijs largoque cruore
Delectos peligisque graues et cursibus haustos
Impiger alterno uigilasset uulnere fratres
Id recolens nunc exultant gaudentque uicissim
Germani ad superos nec inulto funere missi
At quibus imperium uirtus ea contulit ultro
Circustant memores sed quid per singula uersor
Non ne uides spatiosum implentia celum
Publicolam ante oculos tanto cognomine dignum
Preclarum pietate ducem patrieque parentem
Lumina uiscendi cupidus flectebat et ignens
Agmen erat uix stabilem qua uergit ad arcton
Lacteus innumeris redimitus circulus astris
Obstupuit queritque uiros et nomina et actus
Care nepos si cuncta uelim memoranda referre
Altera nox optanda tibi est ait aspice ut omnis
Stella cadit pelago celumque reflectitur et jam
Candidus aurore meditantis surgere uultus
Vibrat et eoa iam sonum diluit unda
Iam pater admouit fugientia sidera nutu
Ostendens uetuitque moras hoc nosce satis sit
Romanas has esse animas quibus una tuende
Cura fuit patrie proprio pars magna cruore
Diffuso has petiit sedes meritoque caducem
Pretulit eternam per acerba pericula uitam

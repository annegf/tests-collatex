FOLIO [1r] .
eT mihi conspicuum meritis belloque tremendum
Musa uirum referes. italis cui fracta sub armis
Nobilis eternum prius attulit africa nomen.
Hunc precor exhausto liceat mihi surgere fontem
Ex helicone sacro dulcis mea cura sorores
Si uobis miranda cano. Iam ruris amici
Prata quidem et fontes / uacuisque silentia campis
Fluminaque et colles / et apricis ocia siluis
Restituit fortuna mihi / Vos carmina uati
Reddite uos animos. // Tuque o certissima mundi
Spes superumque decus / quem secula nostra deorum
Victoremque herebi memorant. quem quina uidemus
Larga per innocuum retegentem uulnera corpus
Auxilium fer summe parens. Tibi multa reuertens
Vertice parnasi referam pia carmina si te
Carmina delectant. uel si minus illa placebunt.
Forte etiam lacrimas / quas sic mens fallitur olim
Fundendas longo demens tibi tempore seruo.
Te quoque trinacrii moderator maxime regni /
Hesperiaeque decus / atque eui gloria nostri /
Iudice quo merui uatumque insede sedere /
Optatasque diu lauros / titulumque poetae /
Te precor oblatum tranquillo pectore munus
Hospicio dignare tuo. Nam cuncta legenti /
Forsitan occurret / uacuas quod mulceat aures
FOLIO [1v]
Peniteatque minus suscepti in fine laboris.
Preterea in cunctos pronum sibi feceris annos
Prosperitatis iter. Quis enim damnare sit ausus
Quod uideat placuisse tibi. Fidentius ista
Arguit expertus nutu quem simplice dignum
Effecisse potes quod non erat. Aspice templis
Dona sacris affixa pauens ut uulgus adorat.
Exime despiciet. quantum tua cura fauori
Fama meo conferre potest. Modo mitis n umbra
Nominis ista tibi durum spretura uenenum
Inuidiae latuisse uelis. ubi nulla uetustas
Interea et nulli rodent mea nomina uermes.
Suscipe iamque precor regum inclite suscipe tandem.
Atque pias extende manus et lumina flecte.
Ipse tuos actus meritis ad sidera tollam
Laudibus / atque alio fortassis carmine quondam
Mors modo me paulo expectet. non longa petuntur.
Nomen et alta canam siculi miracula regis
Non audita procul sed quae modo uidimus omnes
Omnia. nanque solent similis quos cura fatigat
Longius isse retro. tenet hos millesimus annus
Solicitos. pudet hac animos consistere meta.
Nullus ad aetatem propriam respexit. ut erret
Musa parum notos nullo prohibente per annos
Liberior. troiamque ideo canit ille ruente
Ille refert thebas. Iuuenemque occultat achillem.
Ille autem emathiam romanis ossibus implet.
Ipse ego non nostri referam modo temporis acta.
Marte sed ausonio sceleratos funditus afros
Eruere est animus nimiasque retundere uires.
At semper te corde gerens properansque reuerti.
Rex iter hoc ingressus agam tua maxima facta
Non ausus tetigisse prius magis illa trahebant.
Sed tremui me / teque uidens atque omnia librans.
Ingenium tentare libet. si forte secundis
Cesserit auspiciis solidis tunc uiribus alta
Aggrediar. Nanque ipse aderis meque ampla uidebit
FOLIO [2r]
Inclita partenope redeuntem ad menia rursus.
Et romana iterum referentem serta poetam.
Nunc teneras frondes humili de stipite uulsi
Scipiade egregio primos comitante paratus.
Tunc ualidos carpam ramos. tu nempe iuuabis /
Materia generose tua. calamumque labantem
Firmabis. meritumque decus continget amanti
Altera temporibus pulcerrima laurea nostris.
qVe tantis sit causa malis / quae cladis origo
Queritur unde animi. quis tot tolerare coegit
Dura pererrato ualidas furor equore gentes
Europamque dedit libiae libiamque rebellem
Europe. alterno uastandas turbine terras.
Ac mihi causa quidem studii non indiga longi
Occurrit. Radix cunctorum infecta malorum.
Inuidia. unde oriens extrema ab origine mors est
Atque aliena uidens tristi dolor omnia uultu
Prospera. Non potuit florentem cernere romam
Emula cartago. surgenti inuiderat urbi
Sed grauius tulit inde parem. mox uiribus auctam
Vidit. et imperio dominae parere potentis
Ac leges audire nouas et ferre tributum
Edidicit. Tacitis intus sed plena querelis
Plena minis frenum funesta superbia tandem
Compulit excutere et clades geminare receptas.
Angebant dolor atque pudor seruilia passos
Multa uiros. animisque incesserat addita duris
Tristis auaricia et nunquam satiabile uotum.
Permixte spes amborum optatumque duobus
Imperium populis. dignus sibi quisque uideri
Omnia cui subsint / totus cui pareat orbis.
Praeterea damnumque recens / iniuriaque atrox
Insula Sardiniae amissa et trinacria rapta
Atque hyspana nimis populo confinis utrique
Omnibus exposita insidiis aptissima predae
Terra tot infandos longum passura labores.
Haud aliter quam cum medio depressa luporum
Pinguis ouis nunc huc rapidis nunc dentibus illuc
Voluitur. inque tremens partes discerpitur omnes
Bellantum / proprioque madens resupina cruore
FOLIO [2v]
Accessit situs ipse loci. Natura locauit
Se procul aduerso spectantes litore gentes
Aduersosque deos / aduersos moribus Vrbes.
Aduersosque animos / odiosaque numina Vtrinque /
Pacatique nihil. Ventos elementaque prorsus
Obuia. et infesto luctantes equore fluctus.
Ter grauiter certatum studiis / et sanguine multo
Acceptum primo profligatumque secundo
Et bellum si uera notes / Nam tertia nudus
Prelia finis habet modico confecta labore
Maxima nos rerum sequimur mediosque tumultus /
Eximiosque duces / et inenarrabile bellum.
uLtima sidereum iuuenem lassata procellis
Hesperia excussamque graui ceruice catenam
Ausoniumque iugum / romanaque senserat arma.
Iam fuga precipites / longe trans equora penos
Egerat. horruerant animos dextramque tonantis
Fulmineam / moresque ducis / famamque decusque /
Armorumque nouas artes / atque orsa cruentis /
Nobilitata malis. Vix tandem littore mauro
Perfidus ingentem respectans hasdrubal hostem
Tutus erat. Sic uenantum perterritus acrem
Respicit / atque canum ceruus post terga tumultum.
Montis anhela procul de uertice colla reflectens.
Constitit oceano domitor telluris hybere /
Qua labore ambiguus uatum / pelagique columnas
Verbereat herculeas / Vbi fessus mergitur alto
Phebus. et estiuo detergit puluere currum.
Hic ubi non Vis ulla manu mortalis at ipsa
Omnipotens aduersa aditum natura negabat
Constitit. atque auidis prereptum faucibus hostem
Indoluit. Vicisse parum. iam blandior egrum
Non mulcet fortuna animum / cartagine recta
Gloria gestarum sordebat fulgida rerum.
Nempe uidebat adhuc profugum longinqua tuentem
Lentaque semineci uibrantem spicula dextra.
FOLIO [3r]
Turbida quin etiam rumoribus omnia miscens
Fama procul nostro ueniens crescebat ab orbe
Arcibus instantem ausoniis uolitare sub armis
Hannibalem patriaeque faces sub menia ferri /
Illustres cecidisse duces ardere nefandis
Ignibus hesperiam / atque undantia cedibus arua.
Vrgebat uindicta patris / pietasque monebat
Vt ceptum sequeretur opus. nam sanguine seuo
Cesorum cineresque sacros umbrasque parentum /
Placari. atque itala detergi fronte pudorem.
Hic amor assiduum pulsabat pectora clari
Scipiade / in frontem eliciens oculosque iuuenta
Fulgentes / calido generosas corde fauillas.
Anxia nox operosa dies uix ulla quietis
Mora duci. tanta indomito sub pectore uirtus.
hAs inter curas ubi sensim amplexibus atris
Nox hudam laxabat humum titonia quamuis /
Vxor adhuc gelidumque senem complexa foueret /
Nec dum purpureo nitidas a cardine ualuas
Vellere / seu roseas / ause reserare fenestras
Excirent dominum famulae quae secula uoluunt
Fessus et ipse caput posuit. Tum lumina dulcis
Victa sopor clausit celoque emissa silenti
Ymbra ingens. faciesque patris per nubila raptim
Astitit / ostendens caro precordia nato /
Et latus et multa transfixum cuspide pectus.
Diriguit totos juuenis fortissimus artus.
Arrecteque horrore come. Tunc ille pauentem
Corripit et noto permulcens incipit ore.
O decus eternum / generisque amplissima nostri
Gloria et o patriae tandem spes una labanti
Siste metum. memorique animo mea dicta reconde.
Optimus ecce breuem / sed quam nisi despicis / horam
Multa ferat placitura / dedit moderator olimpi.
Ille meis uictus precibus stellantia caeli
Limina / perrarum munus patefecit et ambos
Viuentem penetrare polos permisit. Vt astra
Me duce et obliquos calles / patriaeque labores
FOLIO [3v]
Atque tuos et adhuc terris ignota sororum
Stamina / tu rigido contortum pollice fatum
Aspicias. huc flecte animum. Viden illa sub austro
Menia / et infami periura palatia monte /
Femineis fundata dolisÊ? Viden ampla furentum
ConciliaÊ? et tepido stillantem sanguine turbam ?
Heu nimium nostris urbs insignita ruinis.
Heu nuribus trux terra italis / iterum arma retentas
Fracta semelÊ? uacuisque iterum struis agmina bustis ?
Sic tibrim indomitum segnissime bagrada tendis ?
Sic modo byrsa ferox capitolia despicis alta ?
Experiere iterum. et dominam per uerbera nosces
Is tibi nate labor superest. ea gloria iusto
Marte parem factura deis. haec uulnera iuro
Sacra mihi merito / patriae quibus omne rependi /
Quod dederat / quibus ad superos mauortia uirtus /
Fecit iter / non ulla meos fodientibus artus /
Hostibus. atque abeunte anima / mihi multa dolenti
Occurrisse prius tanti solamina casus.
Quam quod magnanimum post funera nostra uidebam
Vltorem superesse domi. Spes ista leuabat
Inde metus alios / hinc sensum mortis amare.
Talia narrantem percurrit et impia mestis
Vulnera luminibus / totumque a uertice corpus.
Lustrat adusque pedes. at mens pia prominet extra.
Vbertimque fluunt lacrime. nec plura parantem /
Sustinuit. mediisque irrumpens uocibus orsus
Heu heu! quid uideo ? quisnam mihi pectora duro
Confixit mucrone parens ? quae dextra uerendam
Gentibus immerito uiolauit sanguine frontem ?
Dic genitor nil quando queris committere nostris 
Auribus. hec dicens alto radiantia fletu
Sydera uisus erat / sedesque implesse quietas.
Infima si liceat summis equare. marina
Piscis aqua profugus / fluuioque repostus ameno
Non aliter stupeat / si iam dulcedine captum
Vis salis insoliti / et subitus circunstet amaror 
Quam sacer ille chorus stupuit. Namque hactenus ire
FOLIO [4r]
Et dolor / et gemitus / et mens incerta futuri    
Atque metus mortis / mundique miserrima nostri
Milia curarum. rapide quibus optima uitae /
Tempora / et in tenebris meliores ducimus annos.
Illic pura dies quam lux eterna serenat.
Quam nec luctus edax / nec tristia murmura turbant.
Non odia incendunt. noua res / auremque deorum
Insuetus pulsat fragor pietate recessus
Lucis inaccesse / tacitumque impleuerat axem.
aT pater amplexu cupido / precibusque modestis /
Occupat. et grauibus cohibet suspiria dictis
Parce <precor> gemitu. non hunc tempusque locusque /
Exposcunt. sed uisa animum si uulnera tangunt
Vsque adeo. et iuuat patrios cognoscere casus.
Accipe. nam paucis perstringam plurima uerbis.
Sexta per hesperios penitus uictriciam campos
Nostraque signa simul romanaque uiderat estas.
Cum mihi iam bellique moras / curasque peroso /
Consilium teste euentu fortuna dedisti
Magnificum infelix fido / ut cum fratre uiritim /
Solicitum partirer onus. geminumque moranti
Incuterem bello calcar. sic alite leua /
Distrahimur / tandem et scissis legionibus ambo /
Insequimur late sparsis legionibus hostem.
Non dum plena colis / iam stamina nostra sorores
Destituunt fesse. Iam mors sua signa relinquit.
Ilicet imparibus ueriti concurrere fatis
Fraudis opem dubio / poscunt in tempore peni.
Ars ea certa uiris / et nostro cognita damno
Celtarumque animos / quibus auxiliaribus arma /
Fratris ad id steterant precio corrumpere adorti /
Persuasere fugam. nostrorum exempla per euum
Ante oculos gestandi ducum. nec robore freti
Externo / proprio non plus in milite fidant.
Obiicit ille deos ius / fas / et inania uerba.
Raptim abeunt. tacitoque uale. Vis quanta metallo est !
FOLIO [4v]
Dii <pudor> alma fides uni succumbitis auro.
Presidio nudata acies fraterna retrorsum
A uia constituit / notosque recurrere montes.
Hec uisa est spes una duci. premit hostis acerbus
Doctus ad extremum cedenti insistere tergo.
Me quoque iam magno distantem punica tractu
Agmina cingebant que clam nouus auxerat hostis
Improbus insultans. uisum et mihi cedere fato
Nequiquam uetitum caro me iungere fratri
Inferior numero multum tribus undique castris
Vallabar multumque locis urgebar iniquis.
Ferrum aderat. spes nulla fuge quod fata sinebant.
Tempore in angusto durissima pectora ferro
Pandimus. et uafras herebo detrudimus umbras.
Ira / dolorque dabant animos. ars bellica nusquam
Consiliique nihil. Ceu dum uelamina pastor
Fida gerens apibus bellum mouet improbus almis
Nocte sub obscura / trepidant. mox dulcia meste
Excedunt inopi subtracta cubilia cera.
Inde ruunt. ceceque fremunt. sparsoque uolatu
Importuno instant capiti. stat callidus hostis
Inceptique tenax. postque irrita uulnera uictor
Eruit. exptirpatque pie cunabula gentis.
Sicque sola salus miseris et summa uoluptas
Inuisam iaculis / gladioque ultore cohortem
Tundimus et rapidas in uulnere linquimus iras.
Illi ex composito stabant / ceu flantibus haustris /
Aereus consistit erix / atque astriger athlas.
Quid moror? incauti armorum sub nube 
Obruimur. fortuna suum tenet inuida morem
Aduersata pios. gelidus mihi pectore sanguis
Heserat. agnosco insidias mortemque propinquam.
Nec mihi sed patrie metuens pro tempore raptim
Ingredior / dictis cuneos firmare labantes.
Hac uia preclari miles patet ardua leti.
I duce me. quem sepe alias maiore profecto es
Fortuna nunquam fama meliore secutus.
FOLIO [5r]
Non acies ferri facies non obuia mortis.
Terreat. exiguo decus ingens sanguine mauors
Obiicit. et claros illustrat cede nepotes.
Nosce genus. patriamque libens amplectere sortem.
Ignauum fortemque mori < ne tangere damno >
Naturae lex una iubet. breue tempus utrique
Iam licet et terrae pelagique pericula cessent.
Vltro aderit suspecta dies. hoc fortibus unum
Contigit ut leti morerentur. cetera flendo
Turba perit. lacrimasque metu diffundit inertes.
Hora breuis longe testis uenit ultima uite.
Ergo age si latio quicquam de sanguine restat /
Morte palam facito. nam dum fortuna sinebat /
Vicimus. et nostris exibant funera dextris.
At modo corporibus < cedunt quando omnia retro >
Sit satis obstruxisse uiam. per pectora nostra
Perque truces oculos uultusque in morte tremendos
Transcendant talem libet his componere montem.
His claustris uallare aditus. sciat horrida ueros
Barbaries cecidisse uiros et pallida quanquam
Haud spernenda tamen romana cadauera calcet.
Accelera bene nata cohors. in limine mors est
Inuidiosa bonis. Romanas semper ad aras /
Cum lacrimis recolenda piis / et thure perenni.
Talibus incensi coeunt. et grandinis instar
Scissa nube ruunt. in tela micantia primus /
Et circunfusus feror irrediturus in hostes.
Consequitur deuota neci fortissima pubes.
Sternimus et morimur paucis tot milia contra
Quid reliquumÊ? Sed fata pii nunc ultima fratris
Expectas. nec enim hesperia felicior ora.
Ille quidem extremo fati de turbine frustra
Surgere conatus magnae sub mole ruinae
Oppressusque itidem. nec mors magis ulla decebat
Altera quam fratris fuerat. concordia uitae
Mira uel exiguis nunquam interrupta querelis.
Vna domus. uictusque idem mens una duobus
FOLIO [5v]
Et mors una fuit. Locus idem corpora seruat
Amborum ac cineres. hoc tempus ferme sub unum
Venimus. hic nobis nulla iam est cura uetusti
Carceris. ex alto sparsos contemnimus artus.
Odimus et laqueos et uincula nota timemus.
Libertatis onus. quod nunc sumus / illud amamus.
ille autem illacrimans tua me tua care profundo /
Corde premit pietas genitor. Sed mollis inersque /
Vltio uerborum semper fuit. optima rerum.
Dic tamen o / sancte parens an uiuere fratrem /
Teque putem / atque alios quos pridem roma sepultos
Defunctosque uocat. Lente pater ipse loquentem
Risit. et o quanta miseri sub nube iacetisÊ?
Humanumque genus quanta caligine ueri
VoluiturÊ? hec inquit sola est certissima uita.
Vestra autem mors est / quam uitam dicitis. At tu
Aspice germanum. Viden ut contemptor acerbe
Mortis eat ? uiden indomitum sub pectore robur ?
Et uiuum decus et flamantia lumina forti ?
Quin etiam a tergo generosum aspicis agmen ?
Hos mihi defunctos audebit dicere quisquam ?
Et tamen egregios humani sorte tributi
Efflauere animos. ac debita corpora tene
Liquerunt. cernis nitido uenientia contra
Per purum radiare diem leta agmina uultu ?
Iamque ait eximie / nec quicquam dulcius unquam
Hos uidisse oculos nemini. Sed nomina nosse
Est animus. tibi ne genitor contraria mens sit
Per superos ipsumque Iouem / solemque uidentem
Omnia per frigios (si qua est ea cura) penates /
Per si quid patrie uenit huc dulcedinis oro.
Aut ego fallor enim / aut quosdam hoc ex agmine noui.
Et mores / habitusque uirum / faciesque / gradusque /
Insolitum licet ora micent. tamen ora recordor.
Vidi et enim / et patria nuper conuiximus urbe.
Vera quidem memoras. Fraus hunc modo rebus ademit
Punica terrenis. periit congressus iniquis
FOLIO [6r]
Credulus etate heu nimium Marcellus in illa.  
Iste memor finis lateri / latus admouet ultro
Nobiscum ue libens celo spatiatur in amplo.
Crispinus longe sequitur quem perfidus uno
Assumpsisse die tentauerat hostis at illum
Languida dilate tribuerunt. Vulnera morti.
Alter ibi cecidit moriens / ubi furta latebant
Inde leuis recte penetrans huc spiritus illic
Frigida carnifici dimisit membra cruento.
En fabium celo maiestas maxima tanti
Nominis / ac rerum iubet habitare sereno.
Cerne ducem quantum. licet hic cuntator ab omni
Dictus erat populo tamen ingens gloria tardis
Debita consiliis uiget. hunc non flama nec ensis
Eripuit latio. sed dum magis arma premebant
Punica tranquillum tulit huc annosa senectus.
Sed magis ardentemque animis pugnasque frementem
Cerne per insidias indigno funere grachum
Corpore seclusum ualido et pollentibus armis.
Preterea Emilio nimium fors inuida paulo
Aspice magnanimum terebrant quot uulnera pectus
Cannensi romana die defleta supremum
Fata putans renuit cladi superesse. sed ultro
Oblatum contempsit equum. multumque rogantem
Reppulit. et nimium respondit uiximus. at tu
Matte animi uirtute puer discede tuumque
Victurum abde caput. teque ad meliora reserua.
Dic patribus muniant urbem. dic menia firment.
Condiscant extrema pati. namque improba seuas
Ingeminat fortuna minas. hostisque cruentus
Victor adest. Fabio mea uerba nouissima profer
Dic me iussorum memorem uixisse suorum.
Dic memorem (te teste) mori. Sed fata feroxque
Collega ingenti turbarunt cuncta tumultu.
Nuda loco caruit Virtus. tulit impetus illam.
Effuge dum morior ne forsan plura loquendo
Sim tibi causa necis. Dicentem talia ferro
Circunstant / uolat ille leuis. timor alleuat artus
Et plumas adiungit equo. et calcaria planctis.
FOLIO [6v]
Anxia ceu uolucris / ubi nidum callidus anguis 
Obsidet. hinc uise se se subducere morti
Optat. et hinc dubitat / sua dulcia uiscera linquens.
Infelix pietas tandem formidine uicta
Cedit / et incussis serum sibi consulit halis.
Vicinaque tremens respectat ab arbore fatum
Natorum / rabiemque fere. et plangoribus omnem 
Implet anhela nemus. strepituque accurit amico.
Sic ibat juuenis memorandus sepe retrorsum
Lumina mesta ferens - uidet ingens surgere campis /
Naufragium. uidet immitem post publica penum
Funera sacra ducis fodientem pectora duris.
Ictibus. et caelum gemitu pulsabat inani.
Quid moror? innumeram licet internoscere turbam
Cesorum hoc bello iuuenum / patriaeque cadentum.
Scilicet immenso studio dum ledere querit
Ciuibus. atque inopem spoliat dum fortibus urbem /
Compleuit celum nostris ferus hannibal umbris.
tAlia dum memorat genitor / suspiria natus
Alta trahens / licuit <fateor> cognoscere quicquid 
Optabam magis / et uultus spectare meorum.
Cetera ni prohibes nil est sermone secundi
Patris amabilius. Quin tu modo cominus inquit
Alloquere. atque aures quamprimum inuade paratas.
His dictis / tulit ante gradum frontemque modestam /
Demisit. patruumque tenens / sic incipit ore.
O uenerande mihi / uero nunquamque parente
Care minus / si uestra deus dedit ora uidere /
Mortales oculos / alti si limina mundi /
Indignoque mihi clarum reserauit olimpum
Da <precor> exiguam nostris affatibus horam.
Nam breue tempus adest. moneorque in castra reuerti.
Occeani subnixa uadis. ubi maxima calpes
Impendet pelago. celumque cacumine pulsat.
Illhic me romana manent / modo signa ducemque
Expectant. Rapidum hoc tandem stat limite bellum.
Suscipit amplexu juuenem placidissimus heros.
FOLIO [7r]
Atque ita. Si iussu superum mortalia celo
Membra uehis nec enim tam magni muneris autor
Alter erit. sumum hoc equidem tibi contigit uni
Eximiumque decus. quam de te concipiam spem
Dictu difficile est. cui tantam numina uiuo
Concessere uiam. Nam ni diuinus inesset
Spiritus aut cuique hoc homini fortuna dedisset.
Que faciles dispensat opes / arcana uidere /
Celica uenturos longe prenoscere casus /
Et fatum prescire suum spectare beatas
Has animas subterque pedes radiantia solis
Lumina et aduersos tam uastis tractibus axes.
Hec nunquam fortuna dabit quia cuncta potenti
Sunt seruata deo qui si te lumine tanto
Illustrat quonam te alii dignantur honoreÊ?
Non ergo immeritos fractos / passimque iacentes
Hesperie campis totiens despeximus hostes.
Vidimus et nostre uindictam mortis ab illa
Egregie pietatis habes per secula famam.
Quidlibet hinc aude mecum nam protinus aurem
Inuenis / atque animum uacuum. quin ocius ergo
IngredimurÊ? fandoque breuem consumimus horamÊ?
Dic <ait is> si uita manet post busta quod almus
Testatur genitor. sique hec est uita perennis.
Nostra autem morti similis quid demoror ultra
In terris. quin huc potius quacumque licebit
Euolat assurgens animus tellure relicta?
Non bene sentis ait. deus hoc certissime sanxit
Legibus eternis hominem statione manere /
Corporis edicto donec reuocemur aperto.
Non igitur properare decet. sed ferre modeste
Quantulacumque breuis superant incommoda uite.
Ne iussum spreuisse dei uideare. quod ista
Sunt geniti sub lege homines / ut regna tenerent
Infima. Nanque illis custodia credita terrae
Et rerum / quas terra uehit pelagusque profundum.
Ergo tibi / cunctisque bonis seruandus in ista est
Carne animus propriamque uetandus linquere sedem.
FOLIO [7v]
Nobilibus curis / studioque et amore uidendi
Promineat ni forte foras corpusque relinquat
Ac longe fugiat sensus. seque ingerat astris.
Hic decet egregios animos. hic exitus est / quem
Diuini fecere uiri meliora sequentes.
Sed dum membra uigent < breuis est mora > suscipe nostri
Consilii quid summa uelit. Tu sacra fidemque /
Iustitiamque cole. pietas sit pectoris hospes /
Sancta tui morumque comes / quae debita uirtus
Magna patri patriaeque maior sed maxima summo
Ac perfecta deo. quibus exornata profecto
Vita / uia in celum / est. quae uos huc tramite recto
Tunc reuehat / cum summa dies exemerit illud
Carnis onus / puraeque animam transmiserit aure.
Hoc etiam meminisse uelim nil gratius illi /
Qui celum / terrasque regit / dominoque patrique /
Actibus ex uestris / quam iustis legibus urbes /
Conciliumque hominum sociatum nexibus equis.
Quisquis enim ingenio patriam seu uiribus alte
Sustulerit. sumptisque oppressam adiuuerit armis /
Hic certum sine fine locum / in regione serena
Expectet / uereque petat sibi premia uitae /
Iustitia statuente dei / que nec quid inultum /
Nec premio caruisse sinit. Sic fatus amoris
Admouitque faces auido / stimulosque nepoti.
eCce autem interea uenientum turba / nec ulli
Nota fuit facies habitus tamen omnibus unus
Sydereoque leuis fulgebat lumine amictus.
Augusta pauci procul omnes fronte preibant /
Iam senioque graues et maiestate uerendi.
Hec facies regum est. quos tempora prima tulerunt
Vrbis ait nostre. frons arguit inclita reges.
Romulus ecce prior famosi nominis auctor
Publicus ille parens. cernis dulcissime quantus
Ardor inest animo ? talem uentura petebant
Regna uirum. Venit incessu moderatior alter
Religione noua populum qui temperet acrem.
Hic uirtute pius / patrie / nuribusque sabinis
Insignis / nostramque ideo transuectus in arcem est.
Aspice solicitum monitu / ceu coniugis almas
Instituat leges. et euntem diuidat annum.
FOLIO [8r]
Extulit hunc natura senem. primoque sub euo
Hanc habuit frontem. sic tempora cana genasque.
Tertius ille sequens qua tu nunc uteris omnem
Militiae expressit regum fortissimus artem
Fulmineus uisu. uictus quoque fulmine solo.
Quartus arat muros / et tibridis hostia fundans
Presagus quas totus opes huc conuehat orbis
Bifida primeuo connectens menia ponte.
Frons quinti mihi nota parum / sed suspicor illum.
Quem nobis longe regem dedit alta corinthos.
Ille est haud dubie. uideo tunicasque togasque /
Et fasces / trabeasque graues / sellasque curules /
Atque leues faleras  / et cuncta insignia nostri
Imperii / currusque / et equos pompasque triumphi.
Illum autem numero quem cernis in ordine sextum
Seruilis solio regem transmisit origo.
Et nomen seruile manet sed regia mens est.
Dedecus hic generis uirtute piauit  / et actis.
Condidit hic censum prior. ut se noscere posset
Roma potens. altumque nihil sibi nota timeret.
Finierat. Tunc ille iterat / si lecta recordor
Romuleo cinxisse comas diademate septem
Audieram. totidem cognomina certa tenebam.
Alter ubi est igitur? Fili carissime dixit
Huc et luxus iners / et dura superbia nunquam
Ascendunt. illum sua pessima crimina auerno
Merserunt. atroxque animus / nomenque superbum.
Hunc exposcis enim qui sceptra nouissima rexit
Rex ferus  / et feritate bonus. Nam tristia passe
Hic libertatis primum urbi ingessit amorem.
Quin tu animas letas. melioraque regna tenentes
Cerne cateruatim uere uirtutis amicos.
Tres simul ante alacres / alternaque brachia nexi
Ibant. hos leto celebrabant agmina pulsu
Vmbrarum atque omni deuotum ex ordine uulgus.
Sustitit admirans / quae tanta est gloria dixit
Ista trium? Quis tantus amor connecti euntes ?
Hos idemque parens  / eademque <ait> extulit aluus.
Hinc amor. Hisque ipsis libertas credita quondam
Hinc fauor. Heu iugulos  / et uulnera cruda duorum
FOLIO [8v]
Aspice utrique recens nitet ut generosa cicatrix
Pectore in aduerso. Populorum pugna potentum
Tergeminis mandata uiris / ut sanguine pauco
Scilicet innumere cessarent funera gentis.
Diuisis exercitibus conspectu suorum
Aduersisque oculis sex ultima bella gerebant.
Libertasque nostra tremens similisque cadenti
Vnius ad fatum dubio sub marte pependit.
Vnius est conserta manu. germanus uterque
Occiderat. populoque nimis fortuna fauere
Ceperat albano. nisi tertius ille superstes
Integer / et fratrum mortes / et publica fata
Restituisset. agens uictricia corpora campo /
Donec seiunctos spatiis largoque cruore
Defectos / plagisque graues / et cursibus haustos
Impiger alterno iugulasset uulnere fratres.
Id recolens nunc exultat / gaudentque uicissim
Germani ad superos nunc multo funere missi.
At quibus imperium uirtus ea contulit ultro
Circunstant memores. sed quid per singula uersorÊ?
Milia nonne uides spatiosum implentia celumÊ?
Publicolam ante alios tanto cognomine dignum
Preclarum pietate ducem patrieque parentem.
Lumina uisendi cupidus flectebat. et ingens
Agmen erat iuxta stabilem qua uergit ad arthon
Lacteus innumeris redimitus circulus astris.
Obstupuit / queritque uiros et nomina et actus.
Care nepos si cuncta uelim memoranda referre
Altera nox optanda tibi est /ait. aspice ut omnis
Stella cadit pelago. celumque reflectitur. et iam
Candidus aurore meditantis surgere uultus
Vibrat / et eoa iam somnum diluit unda.
Iam pater admonuit fugientia sidera nutu
Ostendens / uetuitque moras. hoc nosse satis sit /
Romanas has esse animas, quibus una tuende
Cura fuit patriae. proprio pars magna cruore
Diffuso / has petiit sedes / meritoque caduae /
Pretulit eternam per acerba piacula uitam.

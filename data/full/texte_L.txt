FOLIO f._2r https://gallica.bnf.fr/ark:/12148/btv1b107212132/f6.item.zoom
Et michi conspicuum meritis belloque tremendum
Musa uirum referes ytalis cui fracta subarmis
Nobilis eternum prius attulit affrica nomen.
Hunc precor exhausto liceat michi seggere fontem
Ex elycone sacrum  dulcis mea cura sorores.
Si uobis miranda cano : iam ruris amici
Prata quidem et fontes vacuisque silentia campis
Fluminaque et colles et apricis otia siluis
Restituit fortuna michi uos carmina vati
Reddite uos animos tuque o certissima mundi
Spes superumque decus quem secula nostra deorum
Victorem atque herebi memorant quem quina videmus
Larga per innocuum retegentem vulnera corpus
Auxilium fer summe parens tibi multa reuertens
Vertice parnasi referam pia carmina si te
Carmina delectant uel siminus illa placebunt
Forte etiam lacrimas : quas (sic mens fallitur) olim
Fundendas longo demens tibi tempore seruo.
¶Te quoque trinachii moderator maxime regni
Hesperieque decus atque eui gloria nostri
Iudice quo merui vatumque in sede sedere
Optatasque diu lauros titulumque poete
Te precor oblatum tranquillo pectore munus
Hospitio dignare tuo nam cuncta legenti
Forsitan occurret vacuas quod mulceat aures
Peniteatque minus suscepti in fine laboris.
Preterea in cunctos pronum sibi feceris annos
Posteritatis iter quis enim damnare sit ausus
Quod videat placuisse tibi? fidentius ista
Arguit expertus nutu quem simplice dignum
Effecisse potes quod non erat. aspice templis
Dona sacris affixa pauens ut vulgus adoret
Exime despiciet quantum tua clara fauori
Fama meo conferre potest. modo mitis in vmbra
Nominis ista tui dirum spretura venenum
Inuidiae latuisse velis. vbi nulla vetustas
Interea et nulli rodent mea nomina vermes.
Suscipe iamque precor regum inclite suscipe tandem
Atque pias extende manus et lumina flecte.
¶Ipse tuos actus meritis ad sidera tollam
Laudibus. atque alio fortassis carmine quondam.
Mors modo me paulum expectet non longa petuntur.
Nomen et alta canam siculi miracula regis
Non audita procul. sed que modo vidimus omnis
Omnia namque uiuat similis quos cura fatigat
Longius isse retro. tenet hos millesimus annus
FOLIO f._2v https://gallica.bnf.fr/ark:/12148/btv1b107212132/f7.item.zoom
Solicitos pudet hac alios consistere meta
Nullus ad etatem propriam respexit. ut erret
Musa parum notos nullo prohibente per annos
Liberior troiamque adeo canit ille ruentem
Ille refert tebas. iuuenem occultat achillem.
Ille autem emathiam romanis ossibus implet.
Ipse ego non nostri referam modo temporis acta
Marte sed ausonio sceleratos funditus afros
Eruere est animus nimiasque retundere vires.
At semper te corde gerens properansque reuerti
Rex iter hoc ingressus agam. tua maxima facta
Non ausus tetigisse prius. magis illa trahebant :
Sed tremui me teque uidens. atque omnia librans
Ingenium temptare libet si forte secundis
Cesserit auspicijs solidis tunc viribus alta
Aggrediar. namque ipse aderis meque ampla videbit
Inclita parthenope redeuntem ad menia rursus
Et romana iterum referentem serta poetam.
Nunc teneras frondes humili de stipite vulsi
Scipiade egregios primos comitante paratus.
Tunc validos carpam ramos. tu nempe juuabis
Materia generose tua calamumque labantem
Firmabis meritumque decus continget amanti
Altera temporibus pulcherima laurea nostris
Que tantis sit causa malis que cladis origo
Queritur. vnde animi quis tot tolerare coegit
Dura pererrato validas furor equore gentes.
Europamque dedit libie libiamque rebellem
Europe alterno vastandas turbine terras.
Ac michi causa quidem studij non indiga longi
Occurrit. radix cunctorum infecta malorum
Inuidia. vnde oriens extrema aborigine mors est
Atque aliena videns tristi dolor omnia vultu
Prospera non potuit florentem cernere romam
Emula cartago surgenti in viderat vrbi
Sed grauius tulit inde parem mox viribus auctam
Vidit et imperio domine parere potentis.
Ac leges audire nouas et ferre tributum
Edidicit. tacitis frenum funesta superbia tandem
Compulit excutere et clades geminare receptas.
Angebant dolor atque pudor seruilia passos
FOLIO f._3r
Multa viros animisque incesserat addita duris
Tristis auaritia et nunquam satiabile votum
Permixte spes amborum optatumque duobus
Imperium populis. dignus sibi quisque videri 
Omnia cuj subsint totus cui pareat orbis
Preterea dampnumque recens iniuriaque atrox
Insula sardinie amisse et trinachia rapta
Atque hyspana nimis populo confinis utrique
Omnibus exposita insidijs aptissima prede
Terra tot infandos longum passura labores
Haud aliter quam cum medio de prehensa luporum
Pinguis ouis nunc huc rapidis nunc dentibus illuc
Voluitur inque tremens partes discerpitur omnis
Belantum proprioque madens resupina cruore
Accessit situs ipse loci. natura locauit
Se procul aduerso spectantes litore gentes.
Aduersosque animios aduersas moribus urbes.
Aduersosque deos odiosaque nomina utrinque
Pacatique nichil. ventos elementaque prorsus
Obuia et infesto luctantes equore fluctus
Ter grauibus certatum odijs et sanguine multo.
At ceptum primo profligatumque secundo
Est bellum si uera notes nam tertia nudus
Prelia finis habet modico confecta labore
Maxima nos rerum hic sequimur mediosque tumultus
Eximiosque duces et inenarrabile bellum.
Vltima sidereum iuuenem lassata procellis
Hesperia. excussamque graui ceruice cathenam
Ausoniumque iugum romanaque senserat arma.
Iam fuga precipites longe transequora penos
Egerat horruerant animos dextramque tonantis
Fulmineam moresque ducis famamque genusque
Armorumque nouas artes atque orsa cruentis
Nobilitata malis vix tandem litore mauro
Perfidus vrgentem respectans hasdrubal hostem
Tutus erat Sic venantum perterritus acrem
Respicit atque canum ceruus post terga tumultum
Montis anhela procul de uertice colla reflectens.
Constitit occeano domitor telluris hibere
FOLIO f._3v https://gallica.bnf.fr/ark:/12148/btv1b107212132/f8.item.zoom
Qua labor ambiguus vatum pelagique colunnas
Uerberat herculeas. vbi fessus mergitur alto
Phebus. et estiuo detergit puluere currum.
Hic ubi non uis ulla manu mortalis. at ipsa
Omnipotens aduersa aditum natura negabat.
Constitit atque auidis prereptum faucibus hostem
Indoluit. Vicisse parum iam blandior egrum
Non mulcet fortuna animum. cartagine recta
Gloria gestarum sordebat fulgida rerum.
Nempe videbat ad huc profugum longinqua tuentem
Lentaque semineci uibrantem spicula dextra.
Turbida quin etiam rumoribus omnia miscens
Fama procul nostro veniens crescebat abore.
Arcibus instantem ausonijs volitare sub armis
Anibalem. patrieque faces sub menia ferij
Illustres cecidisse duces ardere nefandis
Ignibus hesperiam. atque vndantia cedibus arua.
Vrgebat vindicta patris pietasque mouebat
Ut ceptum sequeretur opus. nam sanguine seuo
Cesorum cineresque sacros umbrasque parentum
Placari atque itala detergi fronte pudorem
Hic amor assiduum pulsabat pectora clari
Scipiade. in frontem eliciens oculosque iuuenta
Fulgentes calido generosas corde fauillas.
Anxia nox. operosa dies. uix ulla quietis
Hora duci tanta indomito sub pectore virtus
Has inter curas vbi sensim amplexibus atris
Nox vdam laxabat humum. titonio quamuis
Vxor adhuc gelidumque senem complexa foueret
Nec dum purpureo nitidas acardine valuas
Uellere. seu roseas ause reserare fenestras
Excirent dominam famule que secula uoluunt
Fessus et ipse caput posuit. tum lumina dulcis
Victa sopor clausit celoque emissa silenti
Vmbra ingens faciesque patris. per nubila raptim
Astitit ostendens caro precordia nato
Et latus et multa transfixum cuspide pectus.
Diriguit totos inuenis fortissimus artus
Arrecteque horrore come tunc ille pauentem
Corripit. et noto permulcens incipit ore
O decus eternum generisque amplissima nostri
Gloria et o patrie tandem spes vna labantis
Siste metum memorique animo mea dicta reconde.
FOLIO f._4r
Optimus ecce breuem sed que nisi despicis horam
Multa ferat placitura dedit moderator olimpi.
Ille meis victus precibus stellantia celi
Limina perrarum munus patefecit et ambos
Viuentes penetrare polos permisit. ut astra
Me duce et obliquos calles patrieque labores
Atque tuos. et ad huc terris ignota sororum
Stamina cum rigido contortum pollice fatum
Aspicias huc flecte animum Viden illa sub austro
Menia. et infami periura palatia monte
Femineis fundata dolis Viden ampla furentum
Concilia et tepido stillantem sanguine turbam?
Heu nimium nostris urbs insignita ruinis
Heu nuribus trux terra ytalis iterum arma retentas
Fracta semel ? uacuisque iterum struis agmina bustis ?
Sic tibrim indomitum segnissime bagrada tempnis?
Sic modo birsa ferox capitolia despicis alta ?
Experiere iterum. et dominam per verbera nosces.
Is tibi nate labor superest. ea gloria iusto
Marte parem factura deis hec uulnera iuro
Sacra michi merito, patrie, quibus omne refudj
Quod dederat quibus ad superos mauortia virtus
Fecit iter. non ulla meos fodientibus artus
Hostibus atque abeunte anima michi multa dolentj
Occurrisse prius tanti solamina casus
Quod quod magnanimum post funera nostra videbam
Vltorem superesse domi. spes ista leuabat
Inde metus alios. hinc sensum mortis amare.
Talia narrantem percurrit et impia mestis
Vulnera luminibus totumque auertice corpus
Lustrat adusque pedes. At mens pia prominet extra
Vbertimque fuluunt lacrime nec plura parantem
Sustinuit medijsque irrumpens vocibus orsus.
Heu heu quid uideo ? quisnam hec michi pectora duro
Confixit mucrone parens ? que dextra verendam
Gentibus immerito uiolauit sanguine frontem ?
Dic genitor nil ante velis, commictere nostris
Auribus hec dicens alto radiantia fletu
Sidera visus erat sedesque implesse quietas.
Infima si liceat summis equare. marina
Piscis aqua profugus fluoque repostus ameno
Non aliter stupeat si iam dulcedine captum
Vis salis insoliti et subitus, circumstet amaror
FOLIO f._4v https://gallica.bnf.fr/ark:/12148/btv1b107212132/f9.item.zoom
Quam sacer ille chorus stupuit namque hactenus ire
Et dolor et gemitus et mens incerta futuri
Atque metus mortis mundique miserrima nostri
Milia curarum rapide quibus optima uite
Tempora. et in tenebris meliores ducimus annos.
Illic pura dies quam lux eterna serenat
Quam nec luctus edax nec tristia murmura turbant.
Non odia incendunt noua res auremque deorum
Insuetus pulsare fragor pietate recessus
Lucis inaccesse tacitumque impleuerat axem
At pater amplexu cupido precibusque modestis
Occupat et grauibus cohibet suspiria dictis.
Parce precor gemitu. non hoc tempusque locusque
Exposcunt sed uisa animum si uulnera tangunt
Vsque adeo iuuat et patrios agnoscere casus
Accipe nam paucis perstringam plurima verbis
Sexta expe hesperios penitus victoria campos
Nostraque signa simul romanaque viderat estas
Cum michi iam bellique moras curasque peroso
Consilium teste euentu fortuna dedisti
Magnificum infelix fido hic cum fratre uiritim
Solicitum partirer onus geminumque morantj
Incuterem bello calcar. sic alite leua
Distrahimur tandem et scissis legionibus ambo
Insequimur late sparsis regionibus hostem.
Nondum plena colis iam stamina nostra sorores
Destituunt fesse iam mors sua signa relinquit.
Illicet imparibus ueriti concurrere fatis
Fraudis opem dubio poscunt intempore peni.
Ars ea certa uiris et nostro cognita dampno
Cetarumque animos quibus auxiliaribus arma
Fratris ad id steterant precio corrumpere adorti
Persuasere fugam nostrorum exempla pereuum
Ante oculos gestanda ducum. ne robore freti
Externo. proprio non plus in milite fidant.
Obicit ille deos ius fas et inania verba.
Raptim abeunt tacitoque vale uis quanta metallo est
Dij pudor ? alma fides vni succumbitis auro.
Presidio nudata acies fraterna retrorsum
Auia constituit notosque recurrere montes.	
Hec uisa est spes vna duci premit hostis acerbus
Doctus ab extremo cedenti insistere tergo.
Me quoque iam magno distantem punica, tractu
Agmina cingebant. que clam nouus hauserat hostis
FOLIO f._5r
Improbus insultans. Visum et michi credere fato.
Ne quicquam vetitum caro me iungere fratri.
Inferior numero multum tribus vndique castris
Vallabar. multumque locis vrgebar iniquis.
Ferrum aderat spes nulla fuge. Quod fata sinebant
Tempore in angusto durissima pectora ferro
Pandimus. et vafras herebo detrudimus vmbras.
Ira dolorque dabant animos ars bellica nusquam
Consiliique nichil. Ceu dum velamina pastor
Fida gerens apibus bellum mouet improbus almis
Nocte sub obscura trepidant mox dulcia meste
Excedunt inopi subtrata cubilia cera.
Inde ruunt ceceque fremunt sparsoque volatu
Importuno instant capiti stat callidus hostis
Inceptique tenax. postque irrita uulnera victor
Eruit extirpatque pie cunabula gentis.
Sic que sola salus miseris et summa voluptas
Inuisam iaculis gladioque ultore cohortem
Tundimus et rapidas in vulnere linquimus iras.
Illi excomposito stabant ceu flantibus austris
Aerius consistit erix. atque astriger athlas.
Quid moror ? incauti armorum sub nube virumque
Obruimur. fortuna suum tenet invida morem.
Auersata pios. gelidus michi pectore sanguis
Heserat. agnosco insidias mortemque propinquam
Nec michi sed patrie metuens protempore raptim
Ingredior dictis cuneos firmare labantes
Hac via preclari miles patet ardua leti.
I duce me quem sepe alias maiore profecto es
Fortuna nunquam fama meliore sequtus
Non acies ferri facies non obuia mortis
Terreat. exiguo decus ingens sanguine mauors
Obicit. et caros illustrat cede nepotes.
Nosce genus patriamque libens amplectere sortem.
Ignauum fortemque mori ne tangere dampno
Nature lex vna iubet. breue tempus utrique
Iam licet et terre pelagique pericula cessent
Vltro aderit suspecta dies. hoc fortibus vnum
Contigit ut leti morerentur. cetera flendo
Turba perit larimasque metu diffundit inertes
Hora breuis longe testis venit ultima uite.
Ergo age si latio quicquam de sanguine restat
FOLIO f._5v https://gallica.bnf.fr/ark:/12148/btv1b107212132/f10.item.zoom
Morte palam facito nam dum fortuna sinebat
Vincimus. et nostris exibant funera dextris.
At modo corporibus cedunt quando omnia retro
Sit satis obstruxisse viam. per pectora nostra
Perque truces oculos multusque in morte tremendos
Transcendant talem libet his opponere montem.
His claustris uallare aditus. Sciat horrida veros
Barbaries cecidisse viros. et pallida quamquam
Haud spernenda tamen romana cadauera calcet.
Accelera bene nata cohors - in limine mors est.
Inuidiosa bonis romanas semper adaras
Cum lacrimis recolenda pijs et thure perenni.
Talibus accensi coeunt et grandinis instar
Scissa nube ruunt. Intela micantia primus
Et circumfusos feror in rediturus in hostes.
Consequitur deuota neci fortissima pubes.
Sternimus et morimur paucis tot milia contra
Quid reliquum ? Sed fata pij nunc ultima fratris
Expectas. neque enim hesperia felicior hora
Ille quidem extremo fati deturbine frustra
Surgere conatus magne sub mole ruine
Oppressusque itidem. nec mors magis ulla decebat
Altera quam fratris. fuerat concordia vite
Mira uel exiguis nunquam interrupta querelis.
Vna domus uictusque idem mens vna duobus
Et mors vna fuit. Locus idem corpora seruat
Amborum ac cineres. huc tempus ferme sub unum
Venimus hic nobis nulla est iactura vetusti
Carceris. exalto sparsos contendimus artus.
Odimus et laqueos et vincula nota timemus
Libertatis onus. quod non sumus illud amamus
Ille autem illacrimans. tua me tua care profundo
Corde premit pietas genitor. sed mollis inersque
Vltio verborum semper fuit optima rerum.
Dic tamen hoc hostem parens. an viuere fratrem
Teque putem atque alios. quos pridem roma sepultos
Defunctosque uocat. Lente pater ipse loquentem
Risit. et o quanta miseri sub nube iacetis
Humanumque genus quanta caligine veri
Voluitur. hec inquit sola est certissima vita
Vestra autem mors est quam uitam dicitis. At tu
FOLIO f._6r
Aspice germanum qualis contemptor acerbe
Mortis eat? Vide indomitum sub pectore robur ?
Et uiuum decus et flammantia lumina fronti ?
Quin etiam atergo generosum aspicis agmen ?
Hos michi defuntos audebit dicere quisquam ?
Et tamen egregios humani sorte tributi
Efflauere animos. ac debita corpora terre
Liquerunt. cernis nitido venientia contra
Per purum radiare diem leta agmina vultu ?
Imo ait eximie. nec quicquam dulcius unquam
Hos uidisse oculos memini. sed omnia nosse
Est animus. tibi ne genitor contraria mens sit.
Per superos ipsumque iouem solemque videntem
Omnia. per frigios siqua est ea cura penates.
Per siquid patrie venit huc dulcedinis oro.
Aut ego fallor enim aut quosdam hoc ex agmine nouj
Et mores habitusque uirum faciesque gradusque
Insolitum licet ora micent tamen ipse recordor
Vidi etenim et patria nuper conuiximus urbe.
Vera quidem memoras. fraus hunc modo rebus ademit
Punica terrenis. perijt congressus iniquo
Credulus etate heu nimium marcellus in illa
Iste memor finis lateri latus admouet ultro
Nobiscum ue libens celo speciatur in amplo.
Crispinus longe sequitur. quem perfidus vno
Absumpsisse die temptauerat hostis. at illum
Languida dilate tribuerunt uulnera morti.
Alter ibi cecidit moriens ubi furta latebant.
Inde leuis recte penetrans. huc spiritus illuc
Frigida carnifici dimisit membra cruento.
En fabium celo maiestas maxima tanti
Nominis et rerum iubet habitare sereno.
Cerne ducem quantum. licet hic cunctator ab omnj
Dictus erat populo tamen ingens gloria tardis
Debita consilijs uiget. hunc non flamma non ensis
Eripuit latio sed dum magis arma premebant
Punica. tranquillum tulit hunc annosa senectus.
Sed magis ardentem que animis pugnasque frementem
Cerne per insidias indigno funere gracchum
Corpore se clusum valido et pollentibus armis
Preterea emilio nimium sors in vida paulo
Aspice magnanimum terebrant quot vulnere pectus
FOLIO f._6v https://gallica.bnf.fr/ark:/12148/btv1b107212132/f11.item.zoom
Cannensi romana die deflecte supremum
Fata putans. renuit cladi superesse. sed vltro
Oblatum contempsit equum multumque rogantem
Reppulit et nimium respondit uiximus. at tu
Macte animi virtute puer discede. tuumque
Uicturum abde caput. teque ad meliora reserua.
Dic patribus muniant vrbem. dic menia firment
Condiscant extrema pati. namque improba seuas
In geminat fortuna minas. hostisque cruentus
Victor adest. fabio mea verba nouissima perfer.
Dic me iussorum memorem uixisse suorum.
Dic memorem te teste mori sed fata feroxque
Collega ingenti turbarunt cuncta tumultu.
Nuda loco caruit uirtus. tulit imnpetus illam.
Effuge dum morior ne forsan plora loquendo
Sin tibi causa necis. dicentem talia ferro
Circunstant. uolat ille leuis. timor alleuat artus
Et plumas adiungit equo et calcaria plantis :
Anxia sic genitrix vbi nidum callidus anguis
Obsidet. hinc uise sese subducare morti
Optat et hinc dubitat sua dulcia viscera linquens.
Infelix pietas tandem formidine victa
Cedit et incussis serum sibi consulit alis
Vicinaque tremens respectat abarbore fatum
Natorum rabiemque fere et plangoribus omne
Implet anhela nemus. strepituque accurrit amico.
Sic ibat iuuenis memorandus sepre retrorsum
Lumina mesta ferens. videt ingens surgere campis
Naufragium. videt inmitem post plublica penum
Funera sacra ducis fodientem pectora duris
Ictibus et celum gemitu pulsabat inanj.
Quid moror ? innumeram licet inter noscere turbam
Cesorum hoc bello iuuenum patrieque cadentum
Scilicet inmenso studio dum ledere querit.
Ciuibus atque inopem spoliat dum fortibus urbem
Compleuit celum nostris ferus hanibal vmbris.
Talia dum genitor memorat suspiria natus
Alta trahens. licuit fateor cognoscere quicquid
Optabam magis et vultus spectare meorum
Cetera ni prohibes nichil est sermone secundi
Patris amabilos. Quin tu modo cominus inquit
Aloquere. atque aures quam primum in vade paratas.
His dictis tulit ante gradum frontemque modestam
Demisit patruumque tenens sic incipit ore.
FOLIO f._7r :
O uenerande mihi. uero nunquamque parente
Care minus. Si uestra deus dedit ora videre
Mortales oculos alti si limina mundj
Indignoque michi clarum reserauit olimpum
Da precor exiguam nostris affatibus horam.
Nam breue tempus adest moneorque in castra reuertj
Occeani subnixa uadis. ubi maxima calpes
Impendet pelago celumque cacumine pulsat.
Illic me romana manent modo signa ducemque
Expectant. rapidum hoc tandem stat limite bellum.
Stupit amplexu iuuenem placidissimus heros.
Atque ita si iussu superum mortalia celo
Membra uehis. nec enim tam magni muneris auctor
Alter erit. summum hoc equidem tibi contigit vni
Eximiumque decus. quam dete copiam spem
Dicta difficile est. cui tantam numina uiuo
Concessere uiam nam ni diuinus inesset
Spiritus homini numquam fortuna dedisset
Que faciles dispensat opes archana videre
Celica. venturos longe prenoscere casus.
Et fatum prescire suum spectare beatas
Has animas subterque pedes radiantia solis
Lumina. et aduersos tam vastis tractibus axes.
Hec nunquam fortuna dabit quia cuncta potentj
Sunt seruata deo. qui si te lumine tanto
Illustrat. quonam te alij dignentur honore ?
Non ergo inmeritum fractos passimque iacentes
Hesperie campis totiens despeximus hostes
Uidimus et nostre uindictam mortis  ab illa
Egregie pietatis habes per secula famam.
Quilibet hin aude mecum. nam protinus aurem
Inuenis atque animum uacuum. quin ocius ergo
Ingredimur. fandoque breuem consumimus horam
Dic ait is si uita manet post busta quod almus
Testatur genitor. sique hec est uera perennis
Nostra autem morti similis, quid demoror ultra
Interris ? quin huc potius quacumque licebit
Euolat assurgens animus tellure relicta ?
Non sic care nepos. deus hoc naturaque sanxit
Legibus eternis hominem in statione manere
Corporis. edicto donec reuocetur aperto.
FOLIO f._8r https://gallica.bnf.fr/ark:/12148/btv1b107212132/f12.item.zoom
Non igitur properare decet. sed ferre modeste
Quantulacumque breuis superant in comoda vite
Ne iussu spreuisse dei videare. quod ista
Sunt geniti sub lege homines ut regna tenerent
Infima. namque illis custodia credita terre
Et rerum quas terra vehit. pelagusque profundum.
Ergo tibi cunctisque bonis seruandus in ista est
Carne animus. propriamque vetandus linquere sedem
Nobilibus curis studioque et amore uidendi.
Promineat ni forte foras corpusque relinquat
Ac longe fugiat sensus seque ingerat astris.
Hic decet egregios animos. hic exitus est quem
Diuini fecere viri meliora sequentes.
Sed dum membra uigent breuis est mora. suscipe nostrj
Consilij quid summa velit tu sacra fidemque
Iustitiam que cole pietas sit pectoris hospes
Sancta tui morumque comes que debita virtus
Magna patri patrie maior sed maxima summo
Ac perfecta deo quibus exornata profecto
Vita uia incelum est. que uos huc tramite recto
Tunc reueheat. cum summa dies exemerit istud
Carnis honus. pureque animam transmiserit aure
Hoc etiam monuisse velim nil gratius illi
Qui celum terrasque tegit dominoque patrique
Actibus ex nostris. quam iustis legibus vrbes
Conciliumque hominum socitum nexibus equis.
Sustulerit sumptisque oppressam iuuerit armis
Hic certum sine fine locum in regione serena
Expectet uereque petat sibi premia uite
Iustitia statuente dei. que nec quid inultum
Nec pretio caruisse sinit. sic fatus amoris
Admonuitque faces auido. stimulosque nepotis
En fabium celo maiestas maxima tanti
Nominis, ac uirtus, et iuppiter ipse reseruat.
Cerne ducem quantum licet hic cunctator ab omnj
Dictus erat populo, tamen ingens gloria tardis
Dedita consilijs uiget ! hunc non flamma non ensis
Eripiet latio ! sed dum magis arma temabunt
Punica. tranquillum, feret huc annosa vetustas
Ecce autem interea venientum turba nec ulli
Nota fuit facies. habitus tamen omnibus vnus
FOLIO f._8r :
Sidereo que leuis fulgebat lumine amictus
Augusta pauci procul omnis forte preibant
Iam senioque graues et maiestate verendj.
Hec acies regum est quos tempora prima tulerunt
Urbis ait nostre. frons arguit inclita reges
Romulus ecce prior. famosi nominis auctor
Publicus ille parens. cernis dulcissime quantus
Ardor inest animo. talem ventura petebant
Regna uirum. Venit incessu moderatior alter
Relligione noua populum qui temperet acrem.
Hic virtute prius patrie curibusque sabinis
Insignis. nostramque ideo transuectus in arce est.
Aspice solicitum monitu ceu coniugis almas
Instituat leges. et euntem diuidat annum.
Extulit hunc natura senem. primoque sub euo
Hanc habuit frontem. sic tempora cana genasque.
Tertius ille ques qua tu nunc vteris omnem
Militie expressit regum fortissimus artem
Fulmineus uisu uictus quoque fulmine solo.
Quartus erat muros. et tibridius hostia fundat.
Presagus quas totus opes huc conuehat orbis
Addita primeuo connectens menia ponte
Frons quinti michi nota parum sed suspicor illum
Quem nobis regem longe dedit alta corinthus
Ille est. haud dubie. video tunicasque togasque
Et fasces trabeasque graues sellasque curules
Atque leues faleras et cuncta insignia nostri
Imperij currusque et equos pompasque triumphi
Illum autem toto quem cernis in ordine sextum
Seruilius solio regum transmisit origo
Et nomen seruile manet. sed regia mens est.
Dedecus hic generis uirtute piauit et actis
Condidit hic censum prior ut se noscere posset
Roma potens. altumque nichil sibi nota timeret.
Finierat tunc ille iterat si lecta recordor
Romuleo cinsisse comas diademate septem
Audieram. totidem cognomina certa tenebam.
Alter ubi est igitur ? fili carissime dixit
Huc et luxus in hers et dura superbia nunquam
Ascendunt illum sua pessima crimina auerno
Merserunt atroxque animus nomenque superbum.
Hunc exposcis enim qui sceptra nouissima rexit
Rex ferus et feritate bonus nam tristia passe
Hic libertatis primum vrbi ingessit amorem
FOLIO f._8v https://gallica.bnf.fr/ark:/12148/btv1b107212132/f13.item.zoom
Quin tu animam letas. melioraque regna tenentes
Cerne cateruatim uere virtutis amicos
Tres simul ante alacres alternaque brachia nexi
Ibant. hos leto celebrabant agmina plausa
Umbrarum. atque omni deuotum exordine vulgus
Substitit admirans. que tanta est gloria dixit.
Ista trium. quis tantus amor connectit euntes?
Hos idemque parens. eademque ait extulit aluus.
Hinc amor. hisque ipsis libertas credita quondam
Hinc fauor heu iugulos et vulnera cruda duorum
Aspice. utrique recens nitet ut generosa cicatrix.
Pectore in aduerso populorum fata potentum
Tergeminis mandata viris ut sanguine pauco
Scilicet innumere cessarent funera gentis.
Diuisis exercitibus conspeta suorum
Aduersisque oculis. sex ultima bella gerebant.
Libertas tunc nostra tremens similisque cadenti
Vnius adfatum dubio sub marte pependit.
Vnius est asserta manu germanus uterque
Occiderat populoque nimis fortuna fauere
Ceperat albano nisi tertius ille superstes
Integer et fratrum mortes et publica fata
Restituisset agens uictricia corpora campo.
Donec seiunctos spatijs largoque cruore
Defectos plagisque graues et cursibus haustos
Impiger alterno iugulasset vulnere fratres.
Id recolens nunc exultat gaudentque vicissim
Germanj ad superos nec invlto funere missi.
At quibus imperium virtus ea contulit ultro
Circumstant memores. Sed quid per singula versor ?
Milia nonne vides spatiosum implentia celum ?
Publicolam ante alios tanto cognomine dignum
Preclarum pietate ducem patrieque parentem ?
Lumina visendi cupidus flectebat et ingens
Agmen erat iusta. stabilem qui urgit ad arthon
Lacteus innumeris redimitus circulus astris.
Obstupuit queritque viros et nomina et actus.
Care nepos sicuncta velim memoranda referre
Altera nox optanda tibi est ait. aspice ut omnis
Stella cadit pelago celumque reflectitur et iam
Candidus aurore meditantis surgere vultus
Vibrat et oea iam sompnum diluit unda.
Tum pater admonuit fugientia sidera nutu
Ostendens. Vetuitque moras. hoc nosse satis sit
Romanas has esse animas. quibus vna tuende
Cura fuit patrie. proprio pars magna cruore
FOLIO f._9r :
Diffuso has petijt sedes. meritoque caduce
Pretulit eterna per acerba piacula uitam.

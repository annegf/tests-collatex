Ut michi conspicuum meritis belloque tremendum
Musa uirum referes. italis cui fracta sub armis
Nobilis eternum prius attulit affrica nomen :
Hunc precor exhausto liceat mihi suggere fontem
Ex elicone sacrum dulcis mea cura sorores.
Si uobis miranda cano. iam ruris amici
Prata quidem et fontes uacuisque silentia campis
Fluminaque et colles et apricis ocia siluis
Restituit fortuna mihi uos carmina uati
Reddite. uos animos. Tuque o certissima mundi
Spes : superumque decus quem secula nostra deorum
Victorem atque : herebi memorant : quem quina uidemus
Larga per innocuum retegentem uulnera corpus :
Auxilium fer summe parens. tibi multa reuertens
Vertice parnasi referam pia carmina. si te
Carmina delectant. uel si minus ista placebunt :
Forte etiam lacrimas quas sic mens fallitur olim
Fundendas longo demens tibi tempore seruo.
Te quoque trinacrij moderator maxime regni :
Hesperieque decus : atque eui gloria nostri :
Iudice quo merui uatumque in sede sedere :
Optatasque diu lauros : titulumque poete :
Te precor oblatum tranquillo pectore munus
Hospicio dignare tuo. nam cuncta legenti
Forsitan occurret uacuas quod mulceat aures.
Peniteatque minus suscepti in fine laboris.
Preterea in cunctos pronum sibi feceris annos
Posteritatis iter. quis enim damnare sit ausus
Quod uideat placuisse tibi? fidentius ista
Arguit expertus nutu quem simplice dignum
Effecisse potes. quod non erat. aspice templis
Dona sacris affixa pauens ut uulgus adoret :
Exime. despiciet quantum tua clara fauori
Fama meo conferre potest. modo mitis in umbra
Nominis ista tui dirrum spretura uenenum
Inuidie latuisse uelis ubi nulla uetustas
Interea et nulli rodent mea nomina uermes.
Suscipe iamque precor regum inclite. suscipe tandem
Atque pias extende manus. et lumina flecte.
Ipse tuos actus meritis ad sidera tollam.
Laudibus. atque alio fortassis carmine quondam
Mors modo me paulum expectet. non longa petuntur.
Nomen et alta canam siculi miracula regis.
Non audita procul. sed que modo uidimus omnes
Omnia. nanque solent similis quos cura fatigat
Longius ipse retro tenet hos millesimus annus
Solicitos. pudet hac alios consistere meta
Nullus ad etatem propriam respexit ut erret
Musa parum notos nullo prohibente per annos
Liberior. troiamque ideo canit ille ruentem.
Ille refert thebas. iuuenemque occultat achillem.
Ille autem emathiam romanis ossibus implet.
Ipse ego non nostri referam modo temporis acta.
Marte sed ausonio sceleratos funditus affros
Eruere est animus. nimiasque retundere uires.
At semper te corde gerens properansque reuerti
Rex iter ingressus agam : tua maxima facta
Non ausus tetigisse prius. magis illa trahebant.
Sed tremui me teque uidens atque omnia librans.
Ingenium tentare libet : si forte secundis
Cesserit auspicijs : solidis tunc uiribus alta
Aggrediar. namque ipse aderis. meque ampla uidebit
F2v ( seq 8 )Inclita parthonope redeuntem ad menia rursus
Et romana iterum referentem serta poetam.
Nunc teneras frondes humili de stipite uulsi
Scipiade egregio primo comitante paratus
Tunc ualidos carpam ramos. tu nempe iuuabis
Materia generose tua. calamumque labantem
Firmabis. meritumque decus continget amanti.
Altera temporibus pulcherrima laurea nostris.
Quae tantis sit causa malis que cladis origo
Queritur. unde animi. quis tot tolerare coegit
Dura pererrato ualidas furor equore gentes :
Europamque dedit Libie : Libiamque rebellem
Europe. alterno uastandas turbine gentes.
At mihi causa quidem studij non indiga longi
Occurrit. radix cunctorum infecta malorum
Inuidia. unde omnis extrema ab origine mors est.
Atque aliena uidens tristi dolor omnia uultu
Prospera : non potuit florentem cernere romam
Emula carthago. surgenti inuiderat urbi.
Sed grauius tulit inde parem. mox uiribus auctam
Vidit. et imperio domine parere potentis :
Ac leges audire nouas : et ferre tributum
F3 (seq 9)Edidicit. tacitis intus sed plena querellis
Plena minis frenum funesta superbia tandem
Compulit excutere. et clades geminare receptas.
Angebant dolor atque pudor seruilia passos
Multa uiros. animisque incesserat addita duris
Tristis auaricia. et nonquam saciabile uotum
Permixteque spes amborum. optatumque duobus
Imperium populis. dignus sibi quisque uideri
Omnia cui subsint. totus cui pareat orbis.
Preterea damnumque recens iniuriaque atrox
Insula Sardinee amissa. et trinacria rapta.
Atque hispana nimis populo confinis utrique.
Omnibus exposita insidijs aptissima prede.
Terra tot infandos longum passura labores.
Haud aliter : quam cum medio deprensa luporum
Pinguis ouis : nunc huc rapidis nunc dentibus illuc
Voluitur. inque tremens partes discerpitur omnes.
Bellantum. proprioque madens resupina cruore.
Accessit situs ipse loci. natura locauit
Se procul aduersos spectantes littore gentes.
Aduersosque animos. aduersas moribus urbes
Aduersosque deos. odiosaque numina utrinque
Folio 3v (seq 10) Paccatique nichil. uentos elementaque prorsus
Obuia. et infesto luctantes equore fluctus.
Ter grauibus certatum odijs et sanguine multo
At ceptum primo profligatumque secundo.
Est bellum. si uera notes. nam tercia nudus
Prelia finis habet modico confecta labore.
Maxima nos rerum hic sequimur mediosque tumultus
Eximiosque duces et inenarrabile bellum.
Vltima sidereum iuuenem lassata procellis
Hesperia : excussamque graui ceruice catenam
Ausoniumque iugum romanaque senserat arma.
Iam fuga precipites longe trans equora penos
Egerat. horruerant animos dextramque tonantis
Fulmineam. moresque ducis famamque genusque
Armorumque nouas artes atque orsa cruentis.
Nobilitata malis. uix tandem littore mauro
Perfidus urgentem respectans hasdrubal hostem
Tutus erat. sic uenantum perterritus acrem
Respicit atque canum ceruus post terga tumultum
Montis anhella procul de uertice colla reflectens.
Constitit occeano domitor telluris hibere
Qua labor ambiguus uatum pelagique columnas
F4r (se11) Verberat herculeas. ubi fessus mergitur aluo
Phebus. et estiuo detergit puluere currum.
Hic ubi non uis ulla manu mortalis at ipsa
Omnipotens natura aditum aduersa negabat
Constitit atque auidis prereptum faucibus hostem
Indoluit. uicisse parum iam blandior egrum
Non mulcet fortuna animum. carthagine recta
Gloria gestarum sordebat fulgida rerum.
Nempe uidebat adhuc profugum longinqua tuentem
Lentaque semineci uibrantem spicula dextra.
Turbida quin etiam rumoribus omnia miscens
Fama procul nostro ueniens crescebat ab orbe
Artibus instantem ausonijs uolitare sub armis
Hanibalem. patrieque faces sub moenia ferri.
Illustres cecidisse duces. ardere nefandis
Ignibus hesperiam. atque undantia cedibus arua.
Vrgebat uindicta patris. pietasque monebat
Vt ceptum sequeretur opus. nam sanguine seuo
Cesorum cineresque sacros umbrasque parentum
Placari atque itala detergi fronte pudorem.
Hic amor assiduum pulsabat pectora clari
Scipiade. in frontem eliciens oculosque iuuenta
F4v (se 12) Fulgentes calido generosas corde fauillas
Anxia nox. oppressa dies. uix ulla quietis
Hora duci. tanta indomito sub pectore uirtus.
Has inter curas ubi sensim amplexibus atris
Nox udam laxabat humum : titonia quamuis
Vxor adhuc gelidumque senem complexa foueret.
Necdum purpureo nitidas a cardine ualuas
Vellere seu roseas ause reserare fenestras
Excirent dominam famule que secula uoluunt.
Fessus et ipse caput posuit. tum lumina dulcis
Victa sopor clausit. celoque emissa silenti
Vmbra ingens faciesque patris per nubila raptim
Astitit. ostendens caro precordia nato.
Et latus. et multa transfixum cuspide pectus.
Diriguit totos iuuenis fortissimus artus
Arrecteque horrore come. Tunc ille pauentem
Corripit et noto permulcens incipit ore
O decus eternum generisque amplissima nostri
Gloria. et o patrie tandem spes una labantis
Siste metum memorique animo mea dicta reconde.
Optimus ecce breuem sed que nisi despicis horam
Multa ferat placitura dedit moderator olimpi.
F5 (se 13) Ille meis uictus precibus stellantia celi
Limina  perrarum munus. et ambos
Viuentes penetrare polos permisit : ut astra
Me duce : et obliquos calles patrieque labores
Atque tuos et adhuc terris ignota sororum
Stamina : tum rigido contortum pollice fatum
Aspicias. huc flecte animum. uiden illa sub austro
Moenia et infami periura palacia monte
Femineis fondata dolis? uiden ampla furentum
Concilia ? et trepido stillantem sanguine turbam?
Heu nimium nostris urbs insignita ruinis
Heu nuribus trux terra Italis iterum arma retentas
Fracta semel uacuisque iterum struis agmina bustis?
Sic tibrim indomitum segnissime bagrada temnis?
Sic modo birsa ferox capitolia despicis alta?
Experiere iterum. et dominam per uerbera nosces.
Is tibi nate labor superest. ea gloria iusto
Marte parem factura deis. hec uulnera iuro
Sacra mihi merito : patrie quibus omne rependi
Quod dederat : quibus ad superos mauortia uirtus
Fecit iter. non ulla meos fodientibus artus
Hostibus atque abeunte anima mihi multa dolenti
F5v (s14) Occurrisse prius tanti solamina casus :
Quam quod magnanimum post funera nostra uidebam
Vltorem superesse domi. Spes ista leuabat
Inde metus alios hinc sensum mortis amare.
Talia  narrantem percurrit et impia mestis
 Vulnera luminibus totumque a uertice corpus
 Lustrat adusque pedes at mens pia prominet extra
 Vbertimque fluunt lacrime. nec plura parantem
 Sustinuit medijsque irrumpens uocibus orsus.
 Heu heu quid uideo? quis nam michi pectora duro
 Confixit mucrone parens? que dextra uerendam
 Gentibus immerito uiolauit sanguine frontem?
 Dic genitor. nil ante uelis committere nostris
 Auribus. hec dicens alto radiantia fletu
 Sidera uisus erat sedesque implesse quietas.
 Infima si liceat summis equare marina
 Piscis aqua profugus fluuioque repostus ameno :
 Non aliter stupeat si iam dulcedine captum
 Vis salis insoliti et subitus circumstet amaror
 Quam sacer ille chorus stupuit. namque hactenus ire
 Et dolor et gemitus et mens incerta futuri
 Atque metus mortis mondique miserrima nostri
Millia curarum rapide quibus optima uite
 Tempora et in tenebris meliores ducimus annos
 Illic pura dies quam lux eterna serenat
 Quam nec luctus edax : nec tristia murmura turbant
 Non odia incendunt. noua res auremque deorum
 Insuetus pulsare fragor pietate recessus
 Lucis inaccesse tacitumque impleuerat axem.
 At pater amplexu cupido precibusque modestis
 Occupat et grauibus cohibet suspiria dictis
 Parce precor gemitu non hoc tempusque locusque
 Exposcunt. sed uisa animum si uulnera tangunt
 Vsque adeo iuuat et patrios agnoscere casus
 Accipe. nam paucis perstringam plurima uerbis.
 Sexta per hesperios penitus uictricia campos
 Nostraque signa simul romanaque uiderat estas :
 Cum mihi iam bellique moras curasque peroso :
 Consilium teste euentu fortuna dedisti
 Magnificum infelix. fido ut cum fratre uiritim
 Solicitum partirer onus geminumque moranti
 Incuterem bello calcar. sic alite leua
 Distrahimur tandem : et scissis legionibus ambo
 Insequimur late sparsis regionibus hostem.
 Nondum plena colis iam stamina nostra sorores
 Destituunt fesse iam mors sua signa relinquit.
 Illicet imparibus ueriti concurrere fatis
 Fraudis opem dubio poscunt in tempore peni.
 Ars ea certa uiris et nostro cognita damno.
 Celtiberumque animos : quibus auxiliaribus arma
 Fratris ad id steterant : precio corrumpere adorti
 Persuasere fugam. nostrorum exempla per euum
 Ante oculos gestanda ducum ne robore freti
 Externo : proprio non plus in milite fidant.
Obicit ille deos ius fas et inania uerba
Raptim abeunt tacitoque uale. uis quanta metallo est
Dij pudor alma fides uni succumbitis auro
Presidio nudata acies fraterna retrorsum
Auia constituit. notosque recurrere montes
Hec uisa est spes una duci. premit hostis acerbus
Doctus ad extremum cedenti insistere tergo.
Me quoque iam magno distantem punica tractu
Agmina cingebant. quedam nouus auxerat hostis
Improbus insultans. uisum michi cedere fato
Nequicquam uetitum caro me iungere fratri
Inferior numero multum tribus undique castris
Vallabar multumque locis urgebar iniquis
 Ferrum aderat spes nulla fuge quod fata sinebant
 Tempore in angusto durissima pectora ferro
Pandimus et uafras herebo detrudimus umbras.
Ira dolorque dabant animos. ars bellica nusquam
Consilijque nihil. ceu dum uellamina pastor
Fida gerens apibus bellum mouet improbus almis.
Nocte sub obscura trepidant mox dulcia meste
Excedunt inopi substrata cubilia cera
Inde ruunt ceceque fremunt sparsoque uolatu
Importuno instant capiti sic callidus hostis
Inceptique tenax postque irrita uulnera uictor.
 Eruit extirpatque pie cunabila gentis
Sic que sola salus miseris et summa uoluptas
Inuisam iaculis gladioque ultore cohortem
Tundimus et rapidas in uulnere linquimus iras. 
Illi ex composito stabant ceu flantibus austris
Aerius consistit herix atque astriger athlas.
 Quid moror? incauti armorum sub nube uirumque
 Obruimur fortuna suum tenet inuida morem
 Aduersata pios. gelidus mihi pectore sanguis
 Heserat. agnosco insidias mortemque propinquam.
 Nec mihi sed patrie metuens pro tempore raptim
Ingredior dictis cuneos firmare labantes.
 Hac uia preclari miles patet ardua leti.
 I duce me : quem sepe alias maiore profecto es
 Fortuna nonquam fama meliore secutus
 Nos acies ferri :  facies non obuia mortis
 Terreat exiguo decus ingens sanguine mauors
Obicit. et caros illustrat cede nepotes.
 Nosce genus patriamque libens amplectere sortem.
Ignauum fortemque mori : ne tangere damno :
Nature lex una iubet. breue tempus utrique
Iam licet et terre pelagique pericula cessent :
Vltro aderit suspecta dies. hoc fortibus unum
Contigit : ut leti morerentur cetera flendo
Turba perit lacrimasque metu diffundit inertes.
 Hora breuis longe testis uenit ultima uite.
 Ergo age si latio quicquam de sanguine restat :
 Morte palam facito. nam dum fortuna sinebat
 Vicimus. et nostris exibant funera dextris.
 At modo corporibus  cedunt quando omnia retro
 Sit satis obstruxisse uiam per pectora nostra
 Perque truces oculos uultusque in morte tremendos
 Transcendant. talem licet his opponere montem.
His claustris uallare aditus. sciat horrida ueros
 Barbaries cecidisse uiros et pallida quamquam 
Haud spernenda tamen Romana cadauera calcet
Accelera bene nata cohors. in limine mors est
 Inuidiosa bonis. romanas semper ad aras
 Cum lacrimis recolenda pijs et thure perenni.
Talibus accensi coeunt et grandinis instar
Scissa nube ruunt. in tella micantia primus
Et circumfusos feror irrediturus in hostes.
Consequitur deuota neci fortissima pubes.
Sternimur et morimur pauci tot millia contra.
Quid reliquum? Sed fata pij nunc ultima fratris
Expectas. neque enim hesperia felicior ora
Ille quidem extremo fati de turbine frustra
Surgere conatus magne sub mole ruine
Oppressusque itidem. nec mors magis ulla decebat :
Altera quam fratris fuerat concordia uite :
Mira : uel exiguis nonquam interrupta querellis :
Vna domus : uictusque idem : mens una duobus
 Et mors una fuit. locus idem corpora seruat
Amborum et cineres. huc tempus ferme sub unum
Venimus. hic nobis nulla est iam cura uetusti
Carceris ex alto sparsos contemnimus artus.
\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9\C9.
Libertatis onus. quod nunc sumus illud amamus.
Ille autem illacrimans tua me tua care profundo
Corde premit pietas genitor. sed mollis inersque
Vltio uerborum semper fuit optima rerum.
Dic tamen hoc o sancte parens : an uiuere fratrem
Teque putem atque alios : quos pridem roma sepultos
Defunctosque uocat .  lente pater ipse loquentem
Risit. et o quanta miseri sub nube iacetis
Humanumque genus quanta caligine ueri
Voluitur hec inquit sola est certissima uita
Vestra autem mors est quam uitam dicitis. at tu
 Aspice germanum. uiden ut contemptor acerbe
 Mortis eat? uiden indomitum sub pectore robur ?
 Et uiuum decus et flammantia lumina fronti?
 Quin etiam a tergo generosum respicis agmen?
 Hos mihi defunctos audebit dicere quisquam?
 Et tamen egregios humani sorte tributi
Efflauere animos ac debita corpora terre
 Liquerunt. cernis nitido uenientia contra
 Perpurum radiare diem leta agmina uultu?
Immo ait admiror nec quicquam dulcius unquam
Hos uidisse oculos memini. Sed nomina nosce
Est animus tibi ne genitor contraria mens sit
Per superos ipsumque iouem solemque uidentem
Omnia per frigios si qua est ea cura penates
Per si quid patrie uenit huc dulcedinis : oro.
Aut ego fallor enim : aut quosdam ex agmine noui
Et mortes habitusque uirum faciesque gradusque
Insolitum licet ora micent : tamen ipse recordor
Vidi et enim et patria nuper conuiximus urbe.
Vera quidem memoras fraus hunc modo rebus ademit
Punica terrenis. perijt congressus iniquo
Credulus etate heu nimium marcellus in illa.
Iste memor finis lateri latus admouet ultro
Nobiscum ue liber celo spaciatur in amplo.
Crispinus longe sequitur quem perfidus uno
Assumpsisse die : tentauerat hostis at illum
Languida dillate tribuerunt uulnera morti.
Alter ibi cecidit moriens ubi furta latebant.
Inde leuis recte penetrans huc spiritus illic
Frigida carnifici dimisit membra cruento.
En fabium celo maiestas maxima tanti
Nominis : ac rerum iubet hoc habitare sereno.
Cerne ducem quantum. licet hic cunctator ab omni
Dictus erat populo tamen ingens gloria tardis
 Debita consilijs uiget hac non flamma non ensis
Eripuit latio sed dum magis arma tonabunt
Punica : tranquillum feret huc annosa senectus.
Sed magis ardentemque animis pugnasque frementem
Cerne per insidias indigno funere grachum
Corpore seclusum ualido et pollentibus armis.
Preterea emilio nimium fors inuida paulo
Aspice magnanimum terebrant quot uulnera pectus!
Canensi romana die defleta supremum
Fata putans renuit cladi superesse sed ultro
Oblatum contempsit equum multumque rogantem
Reppulit et nimium respondit uisimus. at tu
Macte animi uirtute puer discede . tuumque
Victurum abde caput teque ad meliora reserua.
Dic patribus muniant urbem dic moenia firment
Condiscant extrema pati . namque improba seuas
Ingeminat fortuna minas hostisque cruentus
Victor adest. fabio mea uerba nouissima perfer.
Dic me iussorum memorem uixisse suorum.
Dic memorem te teste mori. sed fata feroxque
Collega ingenti turbarunt cuncta tumultu
Nuda loco caruit uirtus tulit impetus illam.
 Effuge dum morior . ne forsan plura loquendo
Sim tibi causa necis. dicentem talia ferro
Circumstant. Volat ille leuis timor alleuat artus
Et plumas adiungit equo et calcaria plantis.
Anxia ceu volucris ubi nidum callidus anguis
Obsidet : hinc uise sese subducere morti
Optat : et hinc dubitat sua uiscera dulcia linquens :
Infelix pietas tandem formidine uicta
Cedit. et incussis serum sibi consulit allis.
Vicinaque tremens respectat ab arbore fatum
Natorum rabiemque fere et plangoribus omne
Implet anhella nemus strepituque accurrit amico.
Sic ibat iuuenis memorandus sepe retrorsum
Lumina mesta ferens . uidet ingens surgere campis
Naufragium . uidet immitem post publica poenum
Funera : sacra ducis fodientem pectora duris
Ictibus et celum gemitu pulsabat inani.
Quid moror? Innumeram licet inter noscere turbam
Cesorum hoc bello iuuenum patrieque cadentum.
Silicet immenso studio dum ledere querit
Ciuibus : atque inopem spoliat dum fortibus urbem : 
Compleuit celum nostris ferus hanibal umbris.
Talia dum genitor memorat : suspiria natus
Alta trahens licuit fateor cognoscere quicquid
Optabam magis et uultus spectare meorum.
Cetera ni prohibes nihil est sermone secundi
Patris amabilius. quin tu modo cominus inquit
Alloquere : atque auras quamprimum inuade paratas.
His dictis tulit ante gradum frontemque modestam
Demisit patruumque tenens sic incipit ore .
O uenerande mihi uero nonquamque parente
Care minus si uestra deus dedit ora uidere
Mortales oculos alti si limina mondi
Indignoque mihi clarum reserauit olimpum
Da precor exiguam nostris affatibus horam .
Nam breue tempus adest moneorque in castra reuerti.
Occeani subnixa uadis ubi maxima calpes
Impendet pelago celumque cacumine pulsat :
Illic me romana manent modo signa ducemque
Expectant. rapidum hoc tandem stat limite bellum.
Suscipit amplexu iuuenem placidissimus heros
Atque ita : si iussu superum mortalia celo
Membra uehis : nec enim tam magni muneris autor 
Alter erit summum hoc equidem tibi contigit uni
Eximiumque decus . quam de te concipiam spem
Dictu difficile est : cui tantam numina uiuo
Concessere uiam . nam ni diuinus inesset
Spiritus : hoc homini nonquam fortuna dedisset
Que faciles dispensat opes archana uidere
Celica uenturos longe prenoscere casus
Et fatum prescire suum spectare beatas
Has animas subterque pedes radiantia solis
Lumina et aduersos tam uastis tractibus axes
Hec nonquam fortuna dabit quia concta potenti
Sunt seruata deo. qui si te lumine tanto
Illustrat : quonam te alij dignentur honore ?
Non ergo immerito fractos passimque iacentes
Hesperie campis totiens dispeximus hostes
Vidimus et nostre uindictam mortis. ab illa
Egregie pietatis habes per secula famam
Quidlibet hinc aude mecum nam protinus aurem
Inuenis atque animum uacuum. quin ocius ergo
Ingredimur fandoque breuem consumimus horam?
Dic ait is : si uita manet post busta quod almus
Testatur genitor sique hec est uera perennis
Nostra autem morti similis : quid demoror ultra
In terris? quin huc potius quacumque licebit
Euolat assurgens animus tellure relicta?
Non sic care nepos deus hoc naturaque sanxit
Legibus eternis hominem statione manere
Corporis : edicto donec reuocetur aperto.
Non igitur properare decet : sed ferre modeste
Quantulacumque breuis superant incommoda uite
Ne iussum spreuisse dei uideare quod ista
Sunt geniti sub lege homines ut regna tenerent
Infima namque illis custodia credita terre
Et rerum quas terra uehit pelagusque profundum.
Ergo tibi cunctisque bonis seruandus in ista est
Carne animus propriamque uetandus linquere sedem
Nobilibus curis studioque et amore uiuendi
Promineat ni forte foras corpusque relinquat
Ac longe fugiat sensus seque ingerat astris.
Hic decet egregios animos hic exitus est quem
Diuini fecere uiri meliora sequentes.
Sed dum membra uigent : breuis est mora suscipe nostri
Consilii quid summa uelit. tu sacra fidemque
Iusticiamque cole. pietas sit pectoris hospes 
Sancta tui morumque comes que debita uirtus
Magna patri patrie maior sed maxima summo
Ac perfecta deo quibus exornata : profecto
Vita uia in celum est : que uos huc tramite recto
Tunc reuehat : cum summa dies exemerit istud
Carnis onus : pureque animam transmiserit aure.
Hoc etiam monuisse uelim nil gratius illi
Qui celum terrasque regit dominoque patrique
Actibus ex vestris quam iustis legibus urbes
Conciliumque hominum sociatum nexibus equis
Quisquis enim ingenio patriam seu uiribus alte
Sustulerit sumptisque oppressam iuuerit armis :
Hic certum sine fine locum in regione serena
Expectet uereque petat sibi premia uite
Iusticia statuente dei que nec quid inultum
Nil praemio caruisse sinit. sic fatus amoris
Admouitque faces auido stimulosque nepoti.
Ecce autem interea uenientum turba nec ulli
Nota fuit facies habitus tamen omnibus unus :
Sidereoque leuis fulgebat lumine amictus.
Augusta pauci procul omnes fronte preibant
Iam senioque graues et maiestate uerendi. 
Hec acies regum est quos tempora prima tulerunt
Vrbis ait nostre : frons arguit inclita reges.
Romulus ecce prior famosi nominis auctor
Publicus ille parens : cernis dulcissime quantus
Ardor inest animo ? talem uentura petebant
Regna uirum. Venit incessu moderatior alter
Religione noua : populum qui temperet acrem.
Hic uirtute prius : patrie curribusque sabinis
Insignis : nostramque ideo transuectus in arcem est.
Aspice sollicitum monitu ceu coniugis almas
Instituat leges et euntem diuidat annum.
Extulit hunc natura senem primoque sub euo
Hanc habuit frontem sic tempora cana genasque.
Tertius ille sequens qua tu nunc uteris omnem
Militie expressit regum fortissimus artem
Fulmineus uisu uictus quoque fulmine solo.
Quartus arat muros et tibridis hostia fundat
Presagus quas totus opes huc conuehat orbis
Nostraque primeuo connectens menia ponte.
Frons quinti mihi nota parum sed suspicor illum
Quem nobis regem longe dedit alta chorinthus.
Ille est haud dubie uideo tunicasque togasque
Et fasces trabeasque graues sellasque curulles
Atque leues faleras et cuncta insignia nostri
Imperii currusque et equos pompasque triumphi.
Illum autem toto quem cernis in ordine sextum
Seruilis solio regum transmisit origo
Et nomen seruille manet sed regia mens est.
Dedecus hic generis uirtute piauit et actis
Condidit hic censum prior : ut se noscere posset
Roma potens altumque nihil sibi nota timeret.
Finierat. Tunc ille iterat . si lecta recordor
Romuleo cinxisse comas diademate septem
Audieram totidem cognomina certa tenebam.
Alter ubi est igitur? fili carissime dixit
Huc et luxus iners : et dira superbia nonquam
Ascendunt. illum sua pessima crimina auerno
Merserunt atroxque animus nomenque superbum.
Hunc exposcis enim qui sceptra nouissima rexit
Rex ferus et feritate bonus nam tristia passe
Hic libertatis primum urbi ingessit amorem.
Quin animas letas melioraque regna tenentes
Cerne cateruatim uere uirtutis amicos.
F13v (seq 30) Tres simul ante alacres alternaque brachia nexi
Ibant. hos leto celebrabant agmina plausu
Vmbrarum. atque omni deuotum ex ordine uulgus.
Substitit admirans : que tanta est gloria" dixit
Ista trium? quis tantus amor connectit euntes?
Hos idemque parens : eademque ait "extulit aluus.
Hinc amor. Hisque ipsis libertas credita quondam
Hinc fauor. heu iugulos et uulnera cruda duorum
Aspice. utrique recens nitet ut generosa cicatrix.
Pectore in aduerso. Populorum pugna potentum!
Tergeminis mandata uiris ut sanguine pauco
Scilicet innumere cessarent uulnera gentis.
Diuisis exercitibus conspecta suorum
Aduersisque oculis lex ultima bella gerebant
Libertas tunc nostra tremens similisque cadenti
Vnius ad fatum dubio sub marte pependit
Vnius est asserta manu. germanus uterque
Occiderat. populoque nimis fortuna fauere
Ceperat albano : nisi tertius ille superstes
Integer et fratrum mortes et publica fata
Restituisset agens uictricia corpora campo
Donec seiunctos spaciis largoque cruore :
Defectos plagisque graues et cursibus haustos
Impiger alterno iugulasset uulnere fratres.
Id recolens nunc exultat gaudentque uicissim
Germani ad superos nec inulto funere missi.
At quibus imperium uirtus ea contulit ultro
Circumstant memores. sed quid per singula uersor?
Millia nunne uides spaciosum implentia celum?
Publicolam ante alios tanto cognomine dignum
Preclarum pietate ducem patrieque parentem ?
Lumina uisendi cupidus flectebat et ingens
Agmen erat iuxta stabilem qua uergit ad arthon
Lacteus innumeris redimitus circulus astris
Obstupuit queritque uiros et nomina et actus.
Care nepos si cuncta uelim memoranda referre :
Altera nox optanda tibi est ait aspice et omnis
Stella cadit pelago celumque reflectitur et iam
Candidus aurore meditantis surgere uultus
Vibrat et eoa iam somnum dilluit unda.
Tum pater admonuit fugientia sidera nutu
Ostendens uetuitque moras. hoc nosse satis sit
Romanas has esse animas quibus una tuende
Cura fuit patrie. proprio pars magna cruore
Diffuso has petijt sedes meritoque caduce
Pertulit eternam per acerba pericula uitam

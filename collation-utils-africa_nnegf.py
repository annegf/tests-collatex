#!/usr/bin/env python
# coding: utf-8

# # Africa Collation test
# 
# ## Test data
# 
# I have added the exact folios for Bod47, and the first folio for G1 O1 followed by a random folio for test purposes.
# 
# ### Format for folio
# a line containing "FOLIO xx yyy" where [optionnal] xx = folio/page number, and [optionnal] yyy = url. Order is important, no yyy if there is not a xx
# 
# ### Format for linebreak
# Nothing to add, the end of line is read by python in the file.
# 
# 
# ## Conventions in JSON
# 
# What property to add, and how to call them? Here I have folio_no, folio_url, linebreak.
# 
# Folio and line can be either a property, or both a token and a property. so there are four options:
# - folio (number, url) and lines (endline true/false) only property of all tokens
# - folio token, line property
# - folio property, line token
# - folio/line both tokens
# 
# ### normalisation
# I have only added lower case normalisation. We could also add rules for punctuation marks

# In[1]:


import json
import os
import re #annegf
from operator import countOf #annegf
import glob #annegf

# list of paths to textfiles
#TODO: choice between paths, or all files in aa directory
#textfiles = ["data/Bod47.txt", "data/O1.txt", "data/G1.txt"]
dir="tmp/"
print(glob.glob(dir+"*.txt"))
#textfiles = ["data/texte_En_10.txt", "data/texte_F_10.txt", "data/texte_Pg_provisoire_10.txt", "data/texte_L_10.txt", "data/texte_A_10.txt", "data/texte_B_10.txt", "data/texte_Hr_10.txt", "data/texte_K_10.txt", "data/texte_P_10.txt", "data/texte_Tr_10.txt", "data/texte_V_10.txt", "data/texte_W_10.txt", "data/texte_X_10.txt"]
#textfiles = ["data/texte_A.txt", "data/texte_B.txt"]
textfiles = glob.glob(dir+"*.txt")
# converts plain text to json input for CollateX
# folio/linebreak can be either property only (False, by default) 
# or token and property (True)
def collatex_text_to_json(textfiles, folio_is_token=False, token_linebreak=False):
   
    # witnesses list
    listwit = []
    
    # for each file in textfiles
    for path in textfiles:        
        # witness dict
        wit = {}
        
        # get filename
        filename = os.path.basename(path)
        
        # witness identifier (siglum) from filename
        wit['id'] = os.path.splitext(filename)[0]
        
        # list of witness tokens
        wit['tokens'] = []
        
        # open file
        with open(path, 'r', encoding='utf-8') as file:
            
            # folio value
            folio_no = ''
            folio_url = ''
            newfolio = False
            
            # read line by line
            for line in file:
                
                # if folio line
                if line.startswith('FOLIO'):
                    
                    # update folio number and url
                    elem = line.split(' ')
                    FOLIO = elem[0]
                    folio_no = ""
                    folio_url = ""
                    if len(elem) > 1:
                        folio_no = elem[1]
                        
                    if len(elem) > 2:
                        folio_url = elem[2]
                        
                    newfolio = True
                    
                    # if the folio should be a token
                    if folio_is_token:
                        
                        # create token
                        t = {'t': FOLIO, 'n': FOLIO, 'folio_no':folio_no, 'folio_url':folio_url, 'newfolio': newfolio}
                        
                        # append token to the list
                        wit['tokens'].append(t)
                
                else:
                    
                    # separate words at whitespace
                    #listwords = line.split(' ') #annegf commented
                    #annegf tests
                    #listwords = re.split('(\W)', line)
                    #listwords = filter(lambda x: x.strip(), listwords)
                    listwords = re.split('\W', line)
                    listwords = filter(lambda x: x.strip(), listwords)
                    #end annegf tests
                    # for each word
                    for word in listwords:
                         
                        # remove endline
                        token = word.split('\n')
                        
                        # normalisation. Here lowercase. (also possible to add rules for punctuation marks)
                        #n = token[0].lower() #annegf removed
                        n = token[0].lower().replace('u', 'v')
                        #print(token, n)

                        # create a token
                        t = {'t': token[0], 'n': n, 'folio_no':folio_no, 'folio_url':folio_url, 'endline':False if len(token) == 1 else True, 'newfolio': newfolio}
                        
                        # append token to the list
                        wit['tokens'].append(t)
                        
                        # we are not anymore at the start of a new folio
                        newfolio = False
                        
                    # if endline is a token
                    if token_linebreak:
                        t = {'t': 'LINE', 'n': '\n', 'folio_no':folio_no, 'folio_url':folio_url, 'endline':True, 'newfolio': newfolio}
                        wit['tokens'].append(t)
        
    
    
        # add to witnesses list
        listwit.append(wit)
    
    # return output as json
    json_witnesses = {'witnesses': listwit}
    return json_witnesses


# In[2]:


# first test: folio/lines are only properties
#test_json_input = collatex_text_to_json(textfiles) #annegf commented
#result1 = collate(test_json_input, output='json', layout='vertical', segmentation=False) #annegf commented

# second test: both folio/lines are tokens
test_json_input = collatex_text_to_json(textfiles, folio_is_token=True, token_linebreak=True)
#print(test_json_input)

# In[3]:


from collatex import *


result2 = collate(test_json_input, output='json', layout='vertical', segmentation=False)
#print(result2)

# In[4]:


# special html table for Africa example, both folio/line token
#annegf
#html_table = '<table style="border: 1px solid #000000; border-collapse: collapse;" cellpadding="4" border="1"><tr>'#commented
html_table='<table class="table"><thead style="position: sticky;top: 0" class="bg-dark text-light"><tr>'
#end annegf

witnesses = json.loads(result2)['witnesses']
witrange = range(len(witnesses))

for wit in witnesses:
    html_table += '<th class="header" scope="col">'+wit+'</th>'
html_table += '</tr></thead><tbody>'

for row in list(zip(*json.loads(result2)['table'])):
    
    html_table += '<tr>'
    
    # folio lines
    
    # it checks only the first token in the list 
    # (but we assume here only one token per cell because of the segmentation)
    if any(row[i][0]['t'] == 'FOLIO' for i in witrange if row[i] is not None):
        for cell in row:
            if cell is None:
                string = ''
            else:
                folio = "FOLIO" if cell[0]['folio_no'] == '' else cell[0]['folio_no']
                string = folio if cell[0]['folio_url'] == '' else '<a href="'+cell[0]['folio_url']+'">f.'+cell[0]['folio_no']+'</a>'
            
            html_table += '<td style="">'+string+'</td>' if cell is None else '<td style="border-top:5px double;">'+string+'</td>'
    
    # endlines
    elif any(row[i][0]['t'] == 'LINE' for i in witrange if row[i] is not None):
        for cell in row:
            html_table += '<td style="background-color:gold;"></td>'
    
    # others
    else:
        # set background color according to variation
        # if there is an empty cell, there is a variation
        if any(row[i] == None for i in witrange):
            bgcolor = '#FF7F7'
            htmlclass = "diff"
        # otherwise
        else:
            # list of token t values
            t = [row[i][0]['n'] for i in witrange] #annegf changed 't' to 'n'
            # if the list has more than one value, there is a variation
            if len(set(t)) > 1:
                bgcolor = '#FF7F7'
                htmlclass = "diff"
            else:
                bgcolor = ''
                htmlclass = ""
        #annegf
        normalized = []
        for cell in row:
            n = "-" if cell is None else cell[0]['n']
            normalized.append(n)
        #end annegf
        # create html cells
        for cell in row:
            string = '-' if cell is None else cell[0]['t']

            #annegf
            n = '-' if cell is None else cell[0]['n']
            if countOf(normalized, n) == 1:
                string='<b>'+string+'</b>'
                html_table += '<td class="thediff">'+string+'</td>'
            else:
                html_table += '<td class="'+htmlclass+'">'+string+'</td>'
            #end annegf

            #html_table += '<td bgcolor="'+bgcolor+'">'+string+'</td>'#annegf commented
        html_table += '</tr>'

html_table += '</tbody></table>'


# In[5]:


from IPython.core.display import HTML
HTML(html_table)

#annegf
html_file = open("sortie.html","w")
html_file.write('<head><meta charset="utf-8"/><meta name="viewport" content="width=device-width, initial-scale=1"/>\n<!-- Bootstrap CSS -->\n<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"/></head><body><div class="col-10 offset-1">')
html_file.write('<style>.diff { background-color: pink !important;} .thediff { background-color: tomato !important;} .header { position: sticky; top: 0; }</style>')
html_file.write(html_table)
html_file.write('</div></body></html>')
html_file.close()
#end annegf
# In[6]:


# conversion of JSON for compatibility with pycoviz (remove FOLIO/LINE rows?)

